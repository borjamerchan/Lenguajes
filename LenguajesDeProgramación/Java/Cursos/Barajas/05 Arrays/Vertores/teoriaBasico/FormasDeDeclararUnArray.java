package teoriaBasico;
/**
 * @author Borja$
 */
import java.util.Arrays;
import java.util.Scanner;

public class FormasDeDeclararUnArray {
	public static void main(String[] args) {
		
		ArrayConString();
		arrayIndicandoDatos();
		arrayPidiendoDatos();
	}
	
	public static void arrayPidiendoDatos() {
		Scanner sc = new Scanner (System.in);
		
		int[] numerosB = new int[5];
		
			for (int i = 0;i<numerosB.length; i ++ ) {
				System.out.println("Leyendo Numero para la Segunda Array");
				int dato = sc.nextInt();
				numerosB[i] = dato;
			}
			
			for (int i =0 ; i<numerosB.length; i++) {
				System.out.println("Numeros B; " + numerosB[i]);
			}
	}
	
	
	public static void arrayIndicandoDatos(){
	int[] numeros = {1, 2, 3, 4, 5};
		
		for (int i=0;i<numeros.length;i++) {
			System.out.println("Numeros; " + numeros[i] );
		}
	}
	
	public static void ArrayConString() {
		 String[] nombres = {"Juan", "María", "Carlos"};
		 System.out.println("Arreglo de nombres: " + Arrays.toString(nombres));		
		
	}
}
