package teoriaConObjetos;

public class SintaxisConObjetos {

	
	
	private String nombre;
	private int edad;
	

		// Construtores
		public SintaxisConObjetos(String n,int e) {
			
			this.nombre = n;
			this.edad = e;
		}
		
		public SintaxisConObjetos() {
		
		}
			
		// Getters
		public String getNombre() {
			return nombre;
		}
		// Setters 
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		// Getters
		public int getEdad() {
			return edad;
		}
		// Setters 
		public void setEdad(int edad) {
			this.edad = edad;
		}
		
		// Imprimir 
		
		public void ComprobaciónDeEdad(int edad) {
			if (edad >= 18 && edad <= 65) {
				System.out.println("Acceso permitido... ");
				
			}else {
				System.out.println("Acceso Denegado...");

			}
		}
		
		public void imprimirDatos() {
			System.out.println("Tu nombre es  " + nombre + 
					"\nTu edad es de  " + edad
					);
		}
	
	public static void main(String[] args) {
		SintaxisConObjetos Persona = new SintaxisConObjetos();
		
		Persona.setNombre("Borja");
		Persona.setEdad(25);
		Persona.imprimirDatos();
		Persona.ComprobaciónDeEdad(Persona.getEdad());
	}

	
}
