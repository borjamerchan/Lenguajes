package teoriaConObjetos;

/**
 * @author Borja$
 */

import java.util.Scanner;

public class BasicSwitchConObjetos {
	
	private String nombre;
	private int edad;
	private String Rol; 

		// Construtores
		public BasicSwitchConObjetos(String n,int e,String r) {
			this.nombre = n;
			this.edad = e;
			this.Rol = r;
		}
		
		public BasicSwitchConObjetos() {
		
		}
		// Getters
		public String getNombre() {
			return nombre;
		}
		// Setters 
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		// Getters
		public int getEdad() {
			return edad;
		}
		// Setters 
		public void setEdad(int edad) {
			this.edad = edad;
		}

		
		public String getRol() {
			return Rol;
		}

		public void setRol(String rol) {
			Rol = rol;
		}

		//metodos
		public void imprimirDatos() {
			System.out.println("Tu nombre es  " + nombre + 
					"\n\t Tu edad es de  " + edad
					);
		}
		public void ComprobaciónDeEdad(int edad) {
			if (edad >= 18 && edad <= 65) {
				System.out.println("\n\t Acceso permitido... ");
			}else {
				System.out.println("\n\t Acceso Denegado...");

			}
		}
		
		public void Rol() {
			
			try  {
				Scanner entrada = new Scanner(System.in);
				System.out.println("Dime que Rol Eres");
				String Rol = entrada.nextLine();
				
				switch (Rol) {
				    case "admin":
				    	String Contraseña = "";

				    	System.out.println("Introduce la contraseña");
				    	Contraseña = entrada.nextLine();
				    	
				    		if ( Contraseña.equals("root") ) {
				    			System.out.println("\n\t Assceso Permitido...");
				    		}else {
				    			System.out.println("\n\t Assceso Denegado....");
				    		}
				        break;
				    case "Alumnno":
				        System.out.println("Seleccionaste la opción 1.");
				        break;
				    case "Profesor":
				        System.out.println("Seleccionaste la opción 1.");
				        break;
				    default:
				        System.out.println("Seleccionaste una opción no válida.");
				        break;
				}
			entrada.close();
			}catch (Exception e){
				System.err.println("error");
			}
		}
		
			
	public static void main(String[] args) {
		BasicSwitchConObjetos Persona = new BasicSwitchConObjetos();
		
		Persona.setNombre("Borja");
		Persona.setEdad(25);
		
		Persona.imprimirDatos();
		Persona.ComprobaciónDeEdad(Persona.getEdad());
		Persona.Rol();

	}
}
