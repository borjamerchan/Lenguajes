package excepciónPropia;

public class Main {

	public static void main(String[] args) {

		try {
			PedirNumer(2);
			PedirNumer(23);
			
		}catch( ExceptionPropia e){
			System.out.println("Exception Capturada :" + e);
		}
		
	}
	
	 static void PedirNumer(int num) throws ExceptionPropia{
		if (num > 10) {
			
			throw new ExceptionPropia(num);
			
		}
		
		
	}

}
