package times;

import java.time.LocalTime;

public class ImprimirLaHoraActual {

	public static void main(String[] args) {

		
		
		
		
		LocalTime horaActual = LocalTime.now();
        System.out.println("Hora actual: " + horaActual);

        LocalTime horaEspecifica = LocalTime.of(15, 30, 45);
        System.out.println("Hora específica: " + horaEspecifica);

        int hora = horaActual.getHour();
        int minutos = horaActual.getMinute();
        int segundos = horaActual.getSecond();
        System.out.println("Hora: " + hora);
        System.out.println("Minutos: " + minutos);
        System.out.println("Segundos: " + segundos);
		
	}

}
