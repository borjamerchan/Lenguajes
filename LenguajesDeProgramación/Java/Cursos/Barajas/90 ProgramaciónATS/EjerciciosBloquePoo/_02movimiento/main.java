package _02movimiento;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {

		  Scanner lector=new Scanner(System.in);
	        Movimiento coord ;
	        int x,y;
	        char tecla;
	        
	        System.out.println("Digita las coordenadas de inicio\nDigita X: ");
	        	x=lector.nextInt();
	        System.out.println("Digita Y: ");
	        	y=lector.nextInt();
	        	
	        coord=new Movimiento(x,y);
	        do{
	            System.out.println("ARRIBA = W \n"
	            		+ "ABAJO = S \n"
	            		+ "DERECHA = A \n"
	            		+ "IZQUIERDA = D \n"
	            		+ "POSICION ACTUAL = V \n"
	            		+ "SALIR = X \n"
	            		+ "\nDigita hacia donde quieres moverte: ");
	            
	            tecla=lector.next().charAt(0);
	            switch(tecla){
	                case 'W':
	                case 'w':
	                    coord.arriba();
	                    System.out.println("ARRIBA");
	                    System.out.println("x= "+coord.getX()+" y= "+coord.getY()+"\n");
	                break;
	                
	                case 'S':
	                case 's':
	                    coord.abajo();
	                    System.out.println("ABAJO");
	                    System.out.println("x= "+coord.getX()+" y= "+coord.getY()+"\n");
	                break;
	                
	                case 'A':
	                case 'a':
	                    coord.derecha();
	                    System.out.println("DERECHA");
	                    System.out.println("x= "+coord.getX()+" y= "+coord.getY()+"\n");
	                break;
	                
	                case 'D':
	                case 'd':
	                    coord.izquierda();
	                    System.out.println("IZQUIERDA");
	                    System.out.println("x= "+coord.getX()+" y= "+coord.getY()+"\n");
	                break;
	                
	                case 'V':
	                case 'v':
	                    System.out.println("POSICION ACTUAL");
	                    System.out.println("x= "+coord.getX()+" y= "+coord.getY()+"\n");
	                break;
	                
	              
	                default:
	                	System.out.println("Saliendo De Juego...");
	            }
	        }while(tecla!='x');
	    }
	}