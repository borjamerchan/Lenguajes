

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class BufferReader_V1 {

	/*
	 * BufferReader es una CLASE que tiene los métodos de FileReader
	 * y ADEMÁS tiene métodos para leer linea a linea
	 */
	
	public static void main(String[] args) {
		
		//	si por ejemplo exporto en un excel la nota de los alumnos
		//	CSV = Command Separated Values, salen los alumnos separados por comas, etc...
		//	tengo que leer y sacar los datos de los alumnos
		//	por cada linea instancio un alumno y almacena en un arrayList
		//	como el char lee y cuando detecte salto de linea, paro y separo por comas 
		//	(el salto de línea se puede hacer en distintos SO que tienen distinta separaciones de lineas)
		
		
		
		
		
		try (
			FileReader f = new FileReader("BufferadReadder\\ficheroDeEscritura.log");
			BufferedReader br = new BufferedReader(f)
			){
			
			//StringBuilder
			String linea = "";
			
			//mientras no este vacia la línea
			while((linea = br.readLine()) != null) {
				
				//split divide 
				String[] datos = linea.split("\n");
				
				//que saque un listado de los que tienen mas de un 6 en la segunda evaluacion
				//procesar linea
				//if(Float.valueOf(datos[3])>6)
				
				if(Float.valueOf(datos[0]) == 2) {
					
					//los datos del alumno con dni=2
					System.out.println(linea + "\n");
				}
				
				
				
				
				System.out.print(linea + "\n");
				
				
			}
			
			
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		
		
	} // MAIN

} // CLASS