package Teoria;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.Scanner;


/**
 * 
 * @author Borja$
 *
 */

public class A1_ManipulaciónDeArchivosYDirectorios {

	public static void main(String[] args) throws IOException {
    	Scanner sc = new Scanner(System.in);

		
		/*
		 * Manipulación de archivos y directorios:
		 * 
		 * createNewFile(): Crea un nuevo archivo.
		 * mkdir(): Crea un nuevo directorio.
		 * mkdirs(): Crea un nuevo directorio junto con sus directorios padres.
		 * delete(): Borra el archivo o directorio.
		 * renameTo(File dest): Cambia el nombre del archivo o directorio.
		 */
    	
    	
    	String  ruta = "c:\\tmp\\";
		
		File f = new File (ruta);
		
			System.out.println("Manipulación de archivos y directorios:\r\n"
					+ ""
					+ " \n\t 1 createNewFile(): "
					+ " \n\t 2 mkdir(): "
					+ " \n\t 3 mkdirs: "
					+ " \n\t 4 delate(): "
					+ " \n\t 5 renameTo():"
					);
			
			System.out.println("Dime que Opción quieres ...");
			
			int opcion = sc.nextInt();
			
			switch (opcion) {
	            case 1:
	            	
	            	Scanner sc1 = new Scanner(System.in);

	            	System.out.println("Dime Un Directorio");
	        			String fichero = sc1.nextLine();
	        		
	        		f = new File (ruta + fichero); 
	        			f.createNewFile();
	        		
	            	
	            	break;
	            case 2:
	            	
	            	Scanner sc2 = new Scanner(System.in);

	            	System.out.println("Dime Un Directorio");
	        			String Directorio = sc2.nextLine();
	        		
	        		f = new File (ruta + Directorio); 
	        		
	        			f.mkdir();
	            	break;
	            	
	            case 3:
	            	Scanner sc3 = new Scanner(System.in);

	            	System.out.println("Dime Un Directorio");
	        			String Directorio1 = sc3.nextLine();
	        		
	        		f = new File (ruta + Directorio1); 
	        		
	        			f.mkdirs();
	            	break;
	            	
	            case 4:
	            	Scanner sc4 = new Scanner(System.in);

	            	System.out.println("Dime Un Directorio que quieras borrar ");
	        			String D_borrado = sc4.nextLine();
	        		
	        		f = new File (ruta + D_borrado); 
	        			f.delete();
	        			
	            	break;
	            case 5:
	            	
	            	Scanner sc5 = new Scanner(System.in);

	           
	            	break;
	            default:
	                System.out.println("Seleccionaste una opción no válida.");
	                break;
	        }
			
	}

}
