package Teoria;

import java.io.File;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * 
 * @author Borja$
 *
 */

public class A2_InformaciónSobreArchivosYDirectorios {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		String ruta = "C:\\tmp\\";
		
			File f = new File (ruta);
			
		
			
		/*
		 * exists(): Verifica si el archivo o directorio existe.
		 * isFile(): Verifica si el objeto File representa un archivo.
		 * isDirectory(): Verifica si el objeto File representa un directorio.
		 * getName(): Obtiene el nombre del archivo o directorio.
		 * getPath(): Obtiene la ruta del archivo o directorio.
		 * getParent(): Obtiene el directorio padre.
		 * lastModified(): Obtiene la fecha de la última modificación del archivo o directorio.
		 * 
		 */
			
		

			
		System.out.println("Información sobre archivos y directorios:\r\n"
				+ ""
				+ " \n\t 1 .exists(): "
				+ " \n\t 2 .isFile() .isDirectory(): "
				+ " \n\t 3 .getName(): "
				+ " \n\t 4 .getPath(): "
				+ " \n\t 5 .getParent():"
				+ " \n\t 6 .lastModified():\n"
				);
		
		System.out.println("Dime que Opción quieres ...");

		
		int opcion = sc.nextInt();
		
		switch (opcion) {
            case 1:
        		Scanner sc1 = new Scanner(System.in);

     			System.out.println("\n\tDime el archivo O carpeta que Quieras Comprobar");
     			
     			String o = sc1.nextLine();
          	
     			f = new File (ruta + o);
     				
     				System.out.println(f.exists());
            	
            	
            	break;
            case 2:
        		Scanner sc2 = new Scanner(System.in);
        		
    			System.out.println("\n\tDime el archivo O Carpeta que Quieras Comprobar");
    			
    			String archivo = sc2.nextLine();
         	
    			f = new File (ruta + archivo);
    				
    				System.out.println(f.isFile());
    				
    				if (f.isFile()) {
    					
    					System.out.println("");
    				}
            	
            	break;
            case 3:


    			System.out.println("\n\t Obtención del nombre de la carpeta");
    				System.out.println(f.getName());
            	
            	break;
            	
            case 4:
            	
            	System.out.println("\n\t Obtención La Ruta");
					System.out.println(f.getPath());
            	
            	break;
            	
            case 5:


            	System.out.println("\n\t Obtención La Ruta");
					System.out.println(f.getParent());
            	
            	break;
            	
            case 6:
            		System.out.println(f.getAbsolutePath());
            		long milisegundos = f.lastModified();
            		Date fecha1 = new Date(milisegundos);
            		System.out.println("Última modificación (ms) : " + milisegundos);
            		System.out.println("Última modificación (fecha): " + fecha1);
            	
            	
            	break;
            default:
                System.out.println("Seleccionaste una opción no válida.");
                break;
        }	
	}
}
