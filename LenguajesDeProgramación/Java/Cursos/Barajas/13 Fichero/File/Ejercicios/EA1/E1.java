package Ejercicios.EA1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class E1 {
	
	
	
	public static void main(String[] args) {
		
		 Scanner scanner = new Scanner(System.in);
	        
	        while (true) {
	            System.out.print("Introduce una ruta del sistema de archivos (ruta vacía para salir): ");
	            String input = scanner.nextLine();
	            
	            if (input.isEmpty()) {
	                break;
	            }
	            
	            try {
	                File ruta = new File(input);
	                muestraInfoRuta(ruta);
	            } catch (FileNotFoundException e) {
	                System.out.println("La ruta no existe.");
	            }
	            
	            System.out.println();
	        }
	        
	        scanner.close();
	    }
	    
	    public static void muestraInfoRuta(File ruta) throws FileNotFoundException {
	        if (!ruta.exists()) {
	            throw new FileNotFoundException();
	        }
	        
	        if (ruta.isFile()) {
	            System.out.println("[A] " + ruta.getName());
	        } else if (ruta.isDirectory()) {
	            System.out.println("[*] " + ruta.getName());
	            
	            File[] archivos = ruta.listFiles();
	            Arrays.sort(archivos);
	            
	            for (File archivo : archivos) {
	                if (archivo.isDirectory()) {
	                    System.out.println("[*] " + archivo.getName());
	                } else {
	                    System.out.println("[A] " + archivo.getName());
	                }
	            }
	        }
	    }
	}