package Ejercicios.obligatorio;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
	      Scanner teclado = new Scanner(System.in);

	        String ruta = " ";
         
	        while (true) {

	            System.out.print("Introduce una ruta (ruta vacía para salir): ");

	            ruta = teclado.nextLine();

	            
	            if (ruta.isEmpty()) {
	                break;
	                
	            }

	            try {

	                File archivo = new File(ruta);
	                if (archivo.exists()) {

	                    muestraInfoRuta(archivo, true);

	                } else {

	                    System.out.println("El archivo o directorio no existe.");

	                }

	            } catch (FileNotFoundException e) {

	                System.out.println("Error al acceder al archivo o directorio: " + e.getMessage());

	            }

	        }

	        teclado.close();

	    }



	    public static void muestraInfoRuta(File ruta, boolean info) throws FileNotFoundException {

	        if (ruta.isFile()) {

	            System.out.println("[A] " + ruta.getName());

	            if (info) {

	                System.out.println("\tTamaño: " + ruta.length() + " bytes");

	                System.out.println("\tÚltima modificación: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(ruta.lastModified())));

	            }

	        } else if (ruta.isDirectory()) {

	            File[] archivos = ruta.listFiles();

	            Arrays.sort(archivos);

	            List<File> directorios = new ArrayList<>();

	            List<File> ficheros = new ArrayList<>();

	            for (File archivo : archivos) {

	                if (archivo.isDirectory()) {

	                    directorios.add(archivo);

	                } else {

	                    ficheros.add(archivo);

	                }

	            }

	            directorios.addAll(ficheros);

	            for (File archivo : directorios) {

	                if (archivo.isDirectory()) {

	                    System.out.print("[*] ");

	                } else {

	                    System.out.print("[A] ");

	                }

	                System.out.print(archivo.getName());

	                if (info) {

	                    System.out.println();

	                    System.out.println("\n\t - Tamaño Del Archivo: " + archivo.length()  + 
	                    		"\n\t - Última modificación: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(archivo.lastModified()))
	                    		+ "\n\t - Ruta Absoluta " + archivo.getAbsolutePath());
	                    System.out.println();



	                } else {

	                    System.out.println();

	                }

	            }

	        }

	    }

	
	}


