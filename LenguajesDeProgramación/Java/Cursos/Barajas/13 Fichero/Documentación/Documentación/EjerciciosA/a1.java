package Documentación.EjerciciosA;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class a1 {

	public static void main(String[] args) throws IOException {
		
		String rutabase = "C:\\Users\\Borja$\\Tutorial\\Guia\\Lenguajes\\LenguajesDeProgramación\\Java\\Cursos\\Barajas\\T11_Fichero\\src\\Documentación\\EjerciciosA\\Documentos";
		
		File dir = new File(rutabase);
		File[] lista = dir.listFiles();

		
		System.out.println("Directorio");

		// Recorremos el array y mostramos el nombre de cada elemento
		for (int i = 0; i < lista.length; i++) {
				File f = lista[i];
				
			if (f.isDirectory()) {
				ImprimirDatos(f);				
			}	
		}
		
		System.out.println("Fichero: ");

		for (int i = 0; i < lista.length; i++) {
			File f = lista[i];
				
			if (f.isFile()) {
				
				ImprimirDatos(f);
			}	
			
			
		}
		
		
		
	}
	
	
	public static void ImprimirDatos(File f) {
		
		System.out.println("\t[FI] " + f.getName()  + " " +"\n\t\t Ultima Modificación: " + f.lastModified()  +  " \n\t\t Tamaños En Kbytes: "+  f.length()/(1024*1024)+ "mb");
		
		
		
		
		
	}
}


