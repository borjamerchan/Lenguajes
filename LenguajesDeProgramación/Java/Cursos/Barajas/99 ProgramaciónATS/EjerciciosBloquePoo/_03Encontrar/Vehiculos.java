package _03Encontrar;
/**
 * 
 * @author Borja$
 *Ejercicio 3: Construir un programa que dada una serie de vehículos 
 * caracterizados por su marca, modelo y precio, imprima las propiedades del vehículo más barato. Para ello, se deberán leer por teclado las características de cada vehículo y crear una clase que represente a cada uno de ellos.
 */
public class Vehiculos {

	private  String marca;
	private  String modelo;
	private  int precio;
	
	
	
	public Vehiculos(String m, String mo,int precio ) {
		
		this.marca = m;
		this.modelo =mo;
		this.precio = precio;
		
	}



	public String getMarca() {
		return marca;
	}



	public void setMarca(String marca) {
		this.marca = marca;
	}



	public String getModelo() {
		return modelo;
	}



	public void setModelo(String modelo) {
		this.modelo = modelo;
	}



	public int getPrecio() {
		return precio;
	}



	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	
	
	
	public String InprimirDatos() {
		return "Modelo es \n " + getModelo()
				+ "Marca es \n" + getMarca() 
				+ "Precio es \n " + getPrecio();
	}
	
	
	
	
	
	
	
	
}
