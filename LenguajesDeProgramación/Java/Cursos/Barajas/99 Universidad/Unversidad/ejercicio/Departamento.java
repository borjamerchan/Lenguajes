package ejercicio;

import java.util.HashSet;

public class Departamento {

	String nombre;
	Universidad universidad;
	
	HashSet <Profesor> profesores = new HashSet <Profesor>();
	HashSet <Estudiante> estudiantes = new HashSet <Estudiante>();

	
	public void setEstudiante(Estudiante estudiante) {
		estudiantes.add(estudiante);
	}
	
	public Departamento (String nombre) {
		this.nombre = nombre;
	}
	
		// Añadir Profesores
	public void setProfesor(Profesor profesor) {
		profesores.add(profesor);
	}
	

	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Universidad getUniversidad() {
		return universidad;
	}



	public void setUniversidad(Universidad universidad) {
		this.universidad = universidad;
	}

	
}
