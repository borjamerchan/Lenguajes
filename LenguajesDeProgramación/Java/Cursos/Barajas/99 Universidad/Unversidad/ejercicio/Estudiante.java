package ejercicio;

import java.util.HashSet;

public class Estudiante {


	

	String nombre;
	String cod;
	
	HashSet <Curso> cursos = new HashSet<Curso>();
		  
	
	Universidad universidad;
	Departamento departamento;
	
	
	
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Estudiante (String nombre,String cod) {
		this.nombre = nombre;
		this.cod = cod;
		
	}
	
		// Añadir Cursos
	public void SetCurso (Curso curso) {
		cursos.add(curso);
	}
	
	
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getCod() {
		return cod;
	}


	public void setCod(String cod) {
		this.cod = cod;
	}


	public Universidad getUniversidad() {
		return universidad;
	}


	public void setUniversidad(Universidad universidad) {
		this.universidad = universidad;
	}

	
	
}
