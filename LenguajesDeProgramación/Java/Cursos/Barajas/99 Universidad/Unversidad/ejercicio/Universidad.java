package ejercicio;

import java.util.HashSet;
import java.util.Iterator;

public class Universidad {

	String nombre;
	
	HashSet<Departamento> departamentos = new HashSet<Departamento>();
	HashSet<Estudiante> estudiantes = new HashSet<Estudiante>();
	
	public Universidad (String nombre) {
		this.nombre = nombre;
	}
	
	
	public void setDepartamento (Departamento departamento) {
		departamentos.add(departamento);
	}
	
	
	public Departamento buscarDepartamento (String nombre) {
		
		Departamento d = null;
		Iterator <Departamento> iterador = departamentos.iterator();
		while( iterador.hasNext()) {
			
			d = iterador.next();
			
				if (d.getNombre().equals(nombre)) {
					return d;
				}
		}
		
		d = null;
		return d;
	}
	
	
	public String mostrarDepartamentos() {
		
		String dato = "";
		Iterator <Departamento> iterador = departamentos.iterator();
		Departamento temp = null;
		while( iterador.hasNext()) {
			
			temp = iterador.next();
			dato = dato + temp.toString()+"\n";
		}
		return dato;
	}
	
	public void MatricularEstudiante(Estudiante estudiante, Departamento departamento) {
		estudiantes.add(estudiante);
		estudiante.setUniversidad(this);
		
		departamento.setEstudiante(estudiante);
		estudiante.setDepartamento(departamento);
	}
	

	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	 
		// Añadir departamentos
	public void setDepartamentos (Departamento departamento) {
		departamentos.add(departamento);
	}
	
	
		// Añadir un Estudiante
	public void setEstudiante (Estudiante estudiante) {
		estudiantes.add(estudiante);
	}



	
	 
	
}
