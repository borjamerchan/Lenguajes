package teoriaConObjetos;

/**
 * 
 * @author Borja$
 *
 */

public class TiposDeVariablesConObjetos {
	
	private String nombre;
	private int edad;
	

		// Construtores
		public TiposDeVariablesConObjetos(String n,int e) {
			
			this.nombre = n;
			this.edad = e;
		}
		
		public TiposDeVariablesConObjetos() {
		
		}
			
		// Getters
		public String getNombre() {
			return nombre;
		}
		// Setters 
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		// Getters
		public int getEdad() {
			return edad;
		}
		// Setters 
		public void setEdad(int edad) {
			this.edad = edad;
		}
		
		
		// Imprimir 
		
		
		public void imprimirDatos() {
			
			System.out.println("Tu nombre es de" + nombre + 
					"\nTu edad es de  " + edad
					);
			
		}
	
	public static void main(String[] args) {
		TiposDeVariablesConObjetos Persona = new TiposDeVariablesConObjetos();
		
		Persona.setNombre("Borja");
		Persona.setEdad(25);
	
		Persona.imprimirDatos();
		
	}
}
