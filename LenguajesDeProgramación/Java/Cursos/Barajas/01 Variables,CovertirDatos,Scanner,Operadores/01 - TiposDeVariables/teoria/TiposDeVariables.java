package teoria;

/**
 * 
 * @author Borja$
 *
 */

public class TiposDeVariables {

	public static void main(String[] args) {
		
		
	     // Variables enteras
        int edad = 24;
        System.out.println("Edad: " + edad);

        // Variables decimales
        double precio = 29.99;
        System.out.println("Precio: " + precio);

        float altura = 1.75f;
        System.out.println("Altura: " + altura);

        // Variables de caracteres
        char letra = 'A';
        System.out.println("Letra: " + letra);

        // Variables booleanas
        boolean esMayor = true;
        System.out.println("¿Es mayor de edad?: " + esMayor);

        // Variables de cadenas de caracteres
        String nombre = "borja";
        System.out.println("Nombre: " + nombre);
        
        
        
        

	}

}
