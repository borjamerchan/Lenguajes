package teoria;

/**
 * 
 * @author Borja$
 *
 */

import java.util.Scanner;

public class SeñalizarScanner {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		
		System.out.println("indica Su nombre");
		String	nombre = sc.nextLine();

		System.out.println("indica Su edad");
		int edad = sc.nextInt();

		System.out.println("indica su sexo");
        char sexo = sc.next().charAt(0);
        
        
        System.out.println("Su Nombre es --> " +  nombre + 
        		"\n Su Edad es --> " +  edad +
        		"\n Su letra es --> " +  sexo);
        
        
        sc.close();
	}
}
