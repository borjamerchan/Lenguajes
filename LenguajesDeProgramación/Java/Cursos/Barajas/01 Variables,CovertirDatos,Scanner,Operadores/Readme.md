

### Tipos de Variables
				
				

	- [x] Basica
		- String 
		- int
		- char
		- double
		- float
		- boolean 
		
 https://gitlab.com/borjamerchan/Lenguajes/-/blob/master/LenguajesDeProgramaci%C3%B3n/Java/Cursos/Barajas/01%20Variables/TiposDeVariables/teoria/TiposDeVariables.java
		
	- [x] ConObjetos
	
### Covertir un Dato

	- [x] Char A todos
	- [x] Double A todos
	- [x] String A todos
	- [x] Enteros A todos
	- [x] Floants A todos

https://gitlab.com/borjamerchan/Lenguajes/-/tree/master/LenguajesDeProgramaci%C3%B3n/Java/Cursos/Barajas/01%20Variables/ConvertirDato/convertir

### Scanner
	
	
	- [x] Leer un dato de de tipo String
	- [x] Leer un dato de de tipo int
	- [x] Leer un dato de de tipo double
	- [x] Leer un dato de de tipo char
	- [x] Leer un dato de de tipo double
	- [x] Leer un dato de de tipo float
	

 https://gitlab.com/borjamerchan/Lenguajes/-/blob/master/LenguajesDeProgramaci%C3%B3n/Java/Cursos/Barajas/01%20Variables/Scanner/teoria/Se%C3%B1alizarScanner.java
	
	
	
### Operadores	
	
	
	
#### Operadores aritméticos:

	- [ ] +: Suma dos valores.
	- [ ] -: Resta dos valores.
	- [ ] *: Multiplica dos valores.
	- [ ] /: Divide un valor por otro.
	- [ ] %: Calcula el resto de una división.
	
#### Operadores de asignación:

	- [ ] =: Asigna un valor a una variable.
	- [ ] +=: Añade y asigna un valor a una variable (por ejemplo, a += 5 es equivalente a a = a + 5).
	
#### Operadores de comparación:

	- [ ] ==: Comprueba si dos valores son iguales.
	- [ ] !=: Comprueba si dos valores no son iguales.
	- [ ] >: Comprueba si un valor es mayor que otro.
	- [ ] <: Comprueba si un valor es menor que otro.
	- [ ] >=: Comprueba si un valor es mayor o igual que otro.
	- [ ] <=: Comprueba si un valor es menor o igual que otro.
	
#### Operadores lógicos:

	- [ ] &&: Operador AND lógico, devuelve true si ambas expresiones son verdaderas.
	- [ ] ||: Operador OR lógico, devuelve true si al menos una de las expresiones es verdadera.
	- [ ] !: Operador NOT lógico, invierte el valor de una expresión.
	
#### Operadores de incremento/decremento:

	- [ ] ++: Incrementa en uno el valor de una variable.
	- [ ] --: Decrementa en uno el valor de una variable.
	
	
	
	