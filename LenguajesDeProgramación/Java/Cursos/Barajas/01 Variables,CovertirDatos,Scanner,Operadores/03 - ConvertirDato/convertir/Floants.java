package convertir;

/**
 * 
 * @author Borja$
 *
 */

public class Floants {

	public static void main(String[] args) {
		float numeroFloat = 3.14f;

		// Convertir float a int
		int numeroEntero = (int) numeroFloat;

		// Convertir float a String
		String numeroString = Float.toString(numeroFloat);

		// Convertir float a char (usando el valor entero del float)
		char numeroChar = (char) ((int) numeroFloat);

		// Convertir float a double
		double numeroDouble = (double) numeroFloat;
		
		System.out.println(numeroEntero +
				"\n " + numeroString +
				"\n " + numeroDouble +
				"\n " + numeroChar 
				);
	}

}
