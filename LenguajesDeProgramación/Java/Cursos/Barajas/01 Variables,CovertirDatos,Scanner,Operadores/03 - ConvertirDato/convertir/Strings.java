package convertir;

/**
 * 
 * @author Borja$
 *
 */

public class Strings {

	public static void main(String[] args) {
		String numeroString = "12345";

		// Convertir String a int
		int numeroEntero = Integer.parseInt(numeroString);

		// Convertir String a char (tomando el primer carácter)
		char numeroChar = numeroString.charAt(0);

		// Convertir String a float
		float numeroFloat = Float.parseFloat(numeroString);

		// Convertir String a double
		double numeroDouble = Double.parseDouble(numeroString);
		
		System.out.println(numeroEntero +
				"\n " + numeroFloat +
				"\n " + numeroDouble +
				"\n " + numeroChar 
				);
	}

}
