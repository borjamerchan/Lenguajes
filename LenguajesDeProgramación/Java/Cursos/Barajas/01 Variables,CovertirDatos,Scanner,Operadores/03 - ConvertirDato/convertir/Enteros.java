package convertir;

/**
 * 
 * @author Borja$
 *
 */

public class Enteros {

	public static void main(String[] args) {
		int numeroEntero = 42;

		// Convertir int a String
		String numeroString = Integer.toString(numeroEntero);

		// Convertir int a char
		char numeroChar = (char) numeroEntero;

		// Convertir int a float
		float numeroFloat = (float) numeroEntero;

		// Convertir int a double
		double numeroDouble = (double) numeroEntero;
		
		System.out.println(numeroFloat +
				"\n " + numeroString +
				"\n " + numeroDouble +
				"\n " + numeroChar 
				);
		
	}
}
