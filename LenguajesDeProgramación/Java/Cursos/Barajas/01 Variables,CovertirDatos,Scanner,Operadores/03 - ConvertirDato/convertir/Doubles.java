package convertir;

/**
 * 
 * @author Borja$
 *
 */

public class Doubles {

	public static void main(String[] args) {
		double numeroDouble = 3.14159;

		// Convertir double a int
		int numeroEntero = (int) numeroDouble;

		// Convertir double a String
		String numeroString = Double.toString(numeroDouble);

		// Convertir double a char (usando el valor entero del double)
		char numeroChar = (char) ((int) numeroDouble);

		// Convertir double a float
		float numeroFloat = (float) numeroDouble;
		
		
		
		System.out.println(numeroEntero +
				"\n " + numeroString +
				"\n " + numeroFloat +
				"\n " + numeroChar 
				);
		
	}
}
