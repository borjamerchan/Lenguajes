package convertir;

/**
 * 
 * @author Borja$
 *
 */

public class Chars {

	public static void main(String[] args) {
		char numeroChar = 'A';

		// Convertir char a int
		int numeroEntero = (int) numeroChar;

		// Convertir char a String
		String numeroString = Character.toString(numeroChar);

		// Convertir char a float (usando el valor ASCII del carácter)
		float numeroFloat = (float) ((int) numeroChar);

		// Convertir char a double (usando el valor ASCII del carácter)
		double numeroDouble = (double) ((int) numeroChar);
		
		
		System.out.println(numeroEntero +
				"\n " + numeroString +
				"\n " + numeroFloat +
				"\n " + numeroDouble 
				);
	
	}
}
