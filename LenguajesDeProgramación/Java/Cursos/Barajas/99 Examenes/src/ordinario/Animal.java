package ordinario;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Animal {
    private static int nextId = 1;
    private int id;
    private String nombre;
    private int edad;
    private boolean reservado;

    public Animal(String nombre, int edad, boolean reservado) {
        this.id = nextId++;
        this.nombre = nombre;
        this.edad = edad;
        this.reservado = reservado;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public boolean isReservado() {
        return reservado;
    }

    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }
}

class Perro extends Animal {
    public Perro(String nombre, int edad, boolean reservado) {
        super(nombre, edad, reservado);
    }
}

class Gato extends Animal {
    public Gato(String nombre, int edad, boolean reservado) {
        super(nombre, edad, reservado);
    }
}

public class PeluditosApp {
    private List<Animal> animales;


    public PeluditosApp() {
        animales = new ArrayList<>();
    }

    public void añadirAnimal() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("¿Añadir perro o gato? ");
        String opcion = scanner.nextLine().toLowerCase();

        String nombre, edadStr;
        int edad;
        boolean reservado;

        System.out.print("Nombre: ");
        nombre = scanner.nextLine();
        System.out.print("Edad en meses: ");
        edadStr = scanner.nextLine();
        System.out.print("¿Reservado? (S/N): ");
        reservado = scanner.nextLine().equalsIgnoreCase("S");

        try {
            edad = Integer.parseInt(edadStr);
        } catch (NumberFormatException e) {
            System.out.println("La edad debe ser un número válido. Animal no añadido.");
            return;
        }

        Animal animal;
        if (opcion.equals("perro")) {
            animal = new Perro(nombre, edad, reservado);
        } else if (opcion.equals("gato")) {
            animal = new Gato(nombre, edad, reservado);
        } else {
            System.out.println("Opción inválida. Animal no añadido.");
            return;
        }

        animales.add(animal);
        System.out.println("Animal añadido correctamente.");
    }

    public void mostrarAnimales() {
        System.out.println("Listado de todos los animales:");
        for (Animal animal : animales) {
            String tipo = animal instanceof Perro ? "Perro" : "Gato";
            String estado = animal.isReservado() ? "Reservado" : "No reservado";
            System.out.println(animal.getId() + " - " + tipo + ": " + animal.getNombre() + " - " + estado);
        }
    }

    public void mostrarPerrosSinReserva() {
        System.out.println("Listado de perros sin reserva:");
        for (Animal animal : animales) {
            if (animal instanceof Perro && !animal.isReservado()) {
                System.out.println(animal.getId() + " - " + animal.getNombre());
            }
        }
    }

    public void mostrarGatosSinReserva() {
        System.out.println("Listado de gatos sin reserva:");
        for (Animal animal : animales) {
            if (animal instanceof Gato && !animal.isReservado()) {
                System.out.println(animal.getId() + " - " + animal.getNombre());
            }
        }
    }

    public void reservarAnimal() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el identificador del animal a reservar: ");
        int id = scanner.nextInt();

        Animal animal = buscarAnimalPorId(id);
        if (animal != null) {
            animal.setReservado(true);
            System.out.println("Animal reservado correctamente.");
        } else {
            System.out.println("No se encontró ningún animal con el identificador especificado.");
        }
    }

    public void anularReserva() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el identificador del animal para anular la reserva: ");
        int id = scanner.nextInt();

        Animal animal = buscarAnimalPorId(id);
        if (animal != null) {
            animal.setReservado(false);
            System.out.println("Reserva anulada correctamente.");
        } else {
            System.out.println("No se encontró ningún animal con el identificador especificado.");
        }
    }

    public void adoptarAnimal() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el identificador del animal a adoptar: ");
        int id = scanner.nextInt();

        Animal animal = buscarAnimalPorId(id);
        if (animal != null && animal.isReservado()) {
            animales.remove(animal);
            System.out.println("Animal adoptado correctamente.");
        } else {
            System.out.println("No se encontró ningún animal reservado con el identificador especificado.");
        }
    }

    private Animal buscarAnimalPorId(int id) {
        for (Animal animal : animales) {
            if (animal.getId() == id) {
                return animal;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        PeluditosApp app = new PeluditosApp();
        Scanner scanner = new Scanner(System.in);

        int opcion;
        do {
            System.out.println("---- Menú ----");
            System.out.println("1. Añadir animal");
            System.out.println("2. Mostrar todos los animales");
            System.out.println("3. Mostrar listado de perros sin reserva");
            System.out.println("4. Mostrar listado de gatos sin reserva");
            System.out.println("5. Reservar animal");
            System.out.println("6. Anular reserva");
            System.out.println("7. Adoptar animal");
            System.out.println("8. Salir");
            System.out.print("Ingrese una opción: ");
            opcion = scanner.nextInt();
            scanner.nextLine(); // Limpiar el salto de línea

            switch (opcion) {
                case 1:
                    app.añadirAnimal();
                    break;
                case 2:
                    app.mostrarAnimales();
                    break;
                case 3:
                    app.mostrarPerrosSinReserva();
                    break;
                case 4:
                    app.mostrarGatosSinReserva();
                    break;
                case 5:
                    app.reservarAnimal();
                    break;
                case 6:
                    app.anularReserva();
                    break;
                case 7:
                    app.adoptarAnimal();
                    break;
                case 8:
                    System.out.println("Saliendo del programa...");
                    break;
                default:
                    System.out.println("Opción inválida. Inténtelo de nuevo.");
                    break;
            }

            System.out.println();
        } while (opcion != 8);
    }
}
