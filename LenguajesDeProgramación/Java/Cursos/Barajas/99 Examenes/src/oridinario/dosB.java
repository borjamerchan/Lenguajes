package oridinario;

import java.util.Scanner;

public class dosB {

	public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);

	        System.out.print("Ingrese la base: ");
	        double base = sc.nextDouble();

	        System.out.print("Ingrese el exponente: ");
	        int exponente = sc.nextInt();

	        double resultado = calcularPotencia(base, exponente);
	        System.out.println("El resultado de " + base + " elevado a la " + exponente + " es: " + resultado);
	        
	        sc.close();
	    }

	    public static double calcularPotencia(double base, int exponente) {
	        if (exponente == 0) {
	            return 1;
	        } else if (exponente > 0) {
	            return base * calcularPotencia(base, exponente - 1);
	        } else {
	            return 1 / calcularPotencia(base, -exponente);
	        }
	    }
	
	}


