package oridinario;
		
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Persona[] personas = new Persona[3];
        personas[0] = new Persona("Juan", 1);
        personas[1] = new Persona("María", 23);
        personas[2] = new Persona("Pedro", 20);

        Arrays.sort(personas);

        for (Persona persona : personas) {
            System.out.println(persona.getNombre() + ": " + persona.getEdad());
        }
    }
}

