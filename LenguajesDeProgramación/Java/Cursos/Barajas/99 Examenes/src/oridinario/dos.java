package oridinario;

import java.util.Scanner;

public class dos {

		public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingrese la base: ");  int base = scanner.nextInt();

	        System.out.print("Ingrese el exponente: ");
	        int exponente = scanner.nextInt();

	        int resultado = calcularPotencia(base, exponente);
	        System.out.println("El resultado de " + base + " elevado a la " + exponente + " es: " + resultado);
	        

	        double resultado1 = calcularPotenciaConRecusividad(base, exponente);
	        System.out.println("El resultado de [Con Recusividad] " + base + " elevado a la " + exponente + " es: " + resultado1);
	        
	        scanner.close();
	        
	    }

		
		
		
		public static double calcularPotenciaConRecusividad(double base, int exponente) {
	        if (exponente == 0) {
	            return 1;
	        } else if (exponente > 0) {
	            return base * calcularPotenciaConRecusividad(base, exponente - 1);
	        } else {
	            return 1 / calcularPotenciaConRecusividad(base, -exponente);
	        }
	    }
		
	    public static int calcularPotencia(int base, int exponente) {
	        int resultado = 1;
	        for (int i = 0; i < exponente; i++) {
	            resultado *= base;
	        }
	        return resultado;
	    }
	
	}

