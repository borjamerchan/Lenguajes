package oridinario;

import java.util.Scanner;

public class uno {

	 public static void main(String[] args) {
	        
	            Scanner scanner = new Scanner(System.in);
	            System.out.print("Ingrese un número entero: ");
	            String input = scanner.nextLine();

		 
	            try {
	                int numFilas = Integer.parseInt(input);
	                System.out.println("El número ingresado es: " + numFilas);
	    	        
	                if (numFilas % 2 == 0) {
	    	            System.out.println("Error: El número debe ser impar.");
	    	        } else {
	    		        System.out.println();
	    		        for(int altura = 1; altura<=numFilas; altura++){
	    		            for(int blancos = 1; blancos<=numFilas-altura; blancos++){
	    		                System.out.print(" ");
	    		            }
	    		 
	    		            for(int asteriscos=1; asteriscos<=(altura*2)-1; asteriscos++){
	    		                System.out.print("*");
	    		            }
	    		            System.out.println();
	    		        }
	    	        }
		            scanner.close();
	            } catch (NumberFormatException e) {
	                System.out.println("Error: El valor ingresado no es un número entero válido.");
	            }
	    }
	}
