package Ejercicio2;
import java.io.*;
import java.util.*;

public class main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el nombre del archivo de lectura: ");
        String archivoLectura = sc.nextLine();
        System.out.print("Ingrese el nombre del archivo de escritura: ");
        String archivoEscritura = sc.nextLine();

        // Leer contenido del archivo y almacenarlo en una lista
        List<String> lineas = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(archivoLectura));
        String linea;
        while ((linea = br.readLine()) != null) {
            lineas.add(linea);
        }
        br.close();

        // Ordenar la lista alfabéticamente en orden descendente
        Collections.sort(lineas, Collections.reverseOrder());

        // Escribir el contenido de la lista ordenada en un nuevo archivo
        BufferedWriter bw = new BufferedWriter(new FileWriter(archivoEscritura));
        for (String l : lineas) {
            bw.write(l);
            bw.newLine();
        }
        bw.close();

        System.out.println("Archivo ordenado alfabéticamente en " + archivoEscritura);
    }
}