package ejercicio1;

public class YateLujo extends Barco {
    private double potenciaCV;
    private int numCamarotes;

    public YateLujo(String matricula, double eslora, int anioFabricacion, double potenciaCV, int numCamarotes) {
        super(matricula, eslora, anioFabricacion);
        this.potenciaCV = potenciaCV;
        this.numCamarotes = numCamarotes;
    }

    @Override
    public double calcularModulo() {
        return super.calcularModulo() + potenciaCV + numCamarotes;
    }
}