package ejercicio1;

public class Main {
    public static void main(String[] args) {
        // Crear un velero
        Velero velero = new Velero("ABC123", 12.5, 2010, 3);

        // Calcular el módulo del velero
        double moduloVelero = velero.calcularModulo();
        System.out.println("Módulo del velero: " + moduloVelero);

        // Calcular el costo del alquiler
        int numDias = 7; // Supongamos que el usuario desea alquilar el velero por 7 días
        double valorFijo = 2; // Valor fijo actual de 2 euros
        double costoAlquiler = numDias * moduloVelero * valorFijo;
        System.out.println("Costo del alquiler: " + costoAlquiler + " euros");
    }
}