package ejercicio1;

public class EmbarcacionDeportiva extends Barco {
    private double potenciaCV;

    public EmbarcacionDeportiva(String matricula, double eslora, int anioFabricacion, double potenciaCV) {
        super(matricula, eslora, anioFabricacion);
        this.potenciaCV = potenciaCV;
    }

    @Override
    public double calcularModulo() {
        return super.calcularModulo() + potenciaCV;
    }
}