package colegio;

public class Persona {

	private String NomCompleto;
	private int Edad;

	
	public Persona (String NomCompleto,int Edad) {
		this.NomCompleto = NomCompleto;
		this.Edad = Edad;
		
	}

	public String getNomCompleto() {
		return NomCompleto;
	}


	public void setNomCompleto(String nomCompleto) {
		NomCompleto = nomCompleto;
	}


	public int getEdad() {
		return Edad;
	}


	public void setEdad(int edad) {
		Edad = edad;
	}

	@Override
	public String toString() {
		return "Persona [NomCompleto=" + NomCompleto + ", Edad=" + Edad + "]";
	}

	public void MostrarDatos() {
		
	}
}
