package colegio;

public class Alumno extends Persona  {


	private int idDeAlmuno;

	public Alumno(String NomCompleto, int Edad, int idDeAlmuno) {
		super(NomCompleto, Edad);
		this.idDeAlmuno = idDeAlmuno;
	}
	

	public int getIdDeAlmuno() {
		return idDeAlmuno;
	}

	public void setIdDeAlmuno(int idDeAlmuno) {
		this.idDeAlmuno = idDeAlmuno;
	}
	
	
	
	
	public void MostrarDatos() {
		
		System.out.println("Nombre Completos: " + getNomCompleto() + "\n"
				+ "Edad Del Almumno " + getEdad()+ "\n"
						+ "Id Del Almuno " + getIdDeAlmuno() );
	}
	
	
	
	
	
}
