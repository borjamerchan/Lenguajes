┌──────────────────────────────┬────────────────────────────┐
│                              │                            │
│    SSH User Authentication   │ SSH Connection Protocol    │
│                              │                            │
├──────────────────────────────┴────────────────────────────┤
│                 SSH Transport Layer Protocol              │
│                                                           │
├────────────────────────────────────────────────────────┤
│                              TCP                          │
│                                                           │
└───────────────────────────────────────────────────────────┘
