package EjerciciosObligatorios.Excepcion.p7Gato;

public class Gato {
    private String nombre;
    private int edad;
    
    public Gato(String nombre, int edad) throws Exception {
        setNombre(nombre);
        setEdad(edad);
    }
    
    public String getNombre() {
        return nombre;
    }
    
   
    
    public int getEdad() {
        return edad;
    }
    
    
    public void setNombre(String nombre) throws Exception {
        if (nombre.length() < 3) {
            throw new Exception("El nombre del gato tiene que  tener al menos 3 caracteres.");
        }
        this.nombre = nombre;
    }
    
    public void setEdad(int edad) throws Exception {
    	
    	
        if (edad < 0) {
            throw new Exception("la edad No puede estar en negativa!.");
        }
        this.edad = edad;
    }
    
    public void imprimir() {
        System.out.println("Nombre: " + nombre);
        System.out.println("Edad: " + edad);
    }
}