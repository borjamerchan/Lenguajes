package EjerciciosObligatorios.Excepcion.p7Gato;

import java.util.ArrayList;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
    	
    	
    	
        ArrayList<Gato> listaGatos = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int nGatos = 0;
   
        
        
        while (nGatos < 5) {
            try {
                System.out.println("Introduce los NOMBRE  de los gato " + (nGatos + 1) + ":");
                System.out.print("Nombre: ");
                
                String nombre = scanner.nextLine();
                System.out.print("Edad: ");
                int edad = scanner.nextInt();
                
                Gato gato = new Gato(nombre, edad);
                listaGatos.add(gato);
                nGatos++;
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
        
        System.out.println("\nInformación de los gatos:");
        for (Gato gato : listaGatos) {
            gato.imprimir();
            System.out.println();
        }
    }
}
