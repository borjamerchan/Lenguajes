package EjerciciosObligatorios.Excepcion.p6Cuadrado;

public class Cuadrado {
	
  public static void main(String[] args) throws ExcepcionCuadrado {
    System.out.println("Hola Voy a sacar el cuadrado del numero que me has pasado ");
    
    mCuadrado(args);
    
    System.out.println("Adios ya te di el resultado");
  }

  
  public static void mCuadrado(String[] argumentos) throws ExcepcionCuadrado {
	  
	  System.out.println(recuperaCuadrado(argumentos));
	  
	  }
  
  public static int recuperaCuadrado(String[] c) throws ExcepcionCuadrado {
    if (c.length == 0) {
    	throw new ExcepcionCuadrado(c);
    }
       
    if (!c[0].matches("[0-9]+")) {
    	throw new ExcepcionCuadrado(c); 
    }
      
    if (Integer.valueOf(c[0]).intValue() > 1000) {
      throw new ExcepcionCuadrado(c);
    }
    return Integer.valueOf(c[0]).intValue() * Integer.valueOf(c[0]).intValue();
  }
}


