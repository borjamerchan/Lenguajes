package EjerciciosObligatorios.Excepcion.p7Gato;

public class Main {
    public static void main(String[] args) {
        try {
        	
        	
            Gato gato1 = new Gato("Criki", 2);
            gato1.imprimir();
            
            Gato gato2 = new Gato("Ga", -1 ); 
            gato2.imprimir(); 
            
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
}

