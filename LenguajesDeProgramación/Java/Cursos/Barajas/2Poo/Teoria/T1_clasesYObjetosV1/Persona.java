package T1_clasesYObjetosV1;

public class Persona {
	
	
	private String nombre;
	private int edad;
	private char genero;
	
	public Persona(String n, int e, char g) {
	
		this.nombre = n;
		this.edad = e;
		this.genero = g;
	}
		
	// Metodo
	
	public void MostrarDatos() {	
		System.out.println("Nombre: " + nombre + ""
				+ "\nLa Persona Tiene: " + edad + " años"
						+ "\nGenero: " + genero );
	}
	
	
	public void ComprabacionEdad() {
		if ( edad >= 18 ) {
			MostrarDatos();	
		}else {
			error();	
		}
	}
	
	
	
	public void error() {
		System.out.println("Eres Menor. " + "\nNo pudes Comunicarte Con el servidor...");
	}
	

}
