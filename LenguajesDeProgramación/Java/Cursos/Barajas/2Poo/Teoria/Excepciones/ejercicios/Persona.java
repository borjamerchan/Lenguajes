package Excepciones.ejercicios;

public class Persona {

	
	private String nombre;
	private int edad;
	private double nota;
	
	
	public Persona(String nombre, int edad)
	{
		this.nombre = nombre;
		this.edad = edad;
		
		if(edad < 0)
		{
			throw new IllegalArgumentException();
		}

	}

	

	public void set_nota(double nota)
	{
		if(nota < 0 || nota > 10)
		{
			throw new IllegalArgumentException();
		}
		this.nota = nota;
	}

	
}


