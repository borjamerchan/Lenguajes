package java.ComienzoDeJava.src.V1.Variables;

public class TiposVariables {
    public static void main(String[] args) {
        
        // Tipos Variables;
            // (*) Tipos Numericos
                // (*) Int, Integer;
                    int Numero;
                    Integer Numero1 = 0;

                        // (*) Como Se Declara 
                            Numero = 1;
                            int OtroNumero = 2;

                // Short
                    Short NumeroCorto;

                        // (*) Como Se Declara

                // Long 
                    long NumeroLargo;

                        // (*) Como Se Declara
                        
                // byte 
                    byte NumeroCortoX2;

                        // (*) Como Se Declara

                            /* Esta Forma Es Para Si ya Has Creado la Variable Antes; --> */ 
                                NumeroCortoX2 = 2;
                                byte NumeroCortoV3 = 1;


            // (*) Numero Decimales 

                // Double
                    Double NumeroDecimal;

                        // (*) Como Se Declara

                // float
                    float NumeroFloat;

                        // (*) Como Se Declara


            // (*) Tipos AlfaNuméricos

                // Caracter --> Char
                    char Letra;

                        // (*) Como Se Declara
                        
                            /* Esta Forma Es Para Si ya Has Creado la Variable Antes; --> */ 
                                Letra = 'a';
                                char Letras = 'a';

                // Cadena de Caracteres --> String 
                    String palabras;

                        // (*) Como Se Declara

                            // Esta Forma Es Para Si ya Has Creado la Variable Antes; --> 
                                palabras = "Adios";
                                String Palabra = "Hola";


            // (*) Lógicos & Booleanos

                // Boolean --> True & false

                    // Los Booleans Por Defecto Estan Con el Valor True;
                    boolean ValoresLogicos;

                        // (*) Como Se Declara
                                boolean Valorestrue  = true;
                                boolean ValoresFalse = false;

                                System.out.println(Valorestrue);
    }
}
