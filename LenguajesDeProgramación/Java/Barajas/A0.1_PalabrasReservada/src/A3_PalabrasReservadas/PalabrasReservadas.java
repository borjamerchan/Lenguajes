package A3_PalabrasReservadas;

public class PalabrasReservadas {

	public static void main(String[] args) {

		
	/**
	 * 
	 *	abstract	boolean	break	byte	byvalue*
	 *	case	cast*	catch	char	class
	 *	const*	continue	default	do	double
	 *	else	extends	false	final	finally
	 *	float	for	future*	generic*	goto*
	 *	if	implements	import	inner*	instanceof
	 *	int	interface	long	native	new
	 *	null	operator*	outer*	package	private
	 *	protected	public	rest*	return	short
	 *	satatic	super	switch	synchronized	this
	 *	throw	transient	true	try	var*
	 *	void	volatile	while
	 * 
	 * Las palabras reservadas se pueden clasificar en las siguientes categorías:
	 * 
		 * Tipos de datos: boolean, float, double, int, char
		 * Sentencias condicionales: if, else, switch
		 * Sentencias iterativas: for, do, while, continue
		 * Tratamiento de las excepciones: try, catch, finally, throw
		 * Estructura de datos: class, interface, implements, extends
		 * Modificadores y control de acceso: public, private, protected, transient
		 * Otras: super, null, this.
	 */
	}

}
