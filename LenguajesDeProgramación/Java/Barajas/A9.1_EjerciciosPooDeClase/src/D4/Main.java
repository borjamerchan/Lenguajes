package D4;


public class Main {

	public static void main(String[] args) {

		Articulo miarticulo4 = new Articulo();
		
		miarticulo4.setNombre("Platano");
		miarticulo4.setPrecio(20);

		System.out.println( miarticulo4.imprime("Platano"));
		System.out.println( miarticulo4.getPVP());
		System.out.println( miarticulo4.vender(2));
		System.out.println( miarticulo4.almacenar(0));
	}
}
