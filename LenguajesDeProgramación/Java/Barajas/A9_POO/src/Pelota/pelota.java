package Pelota;

public class pelota {
	
	private double radio;
	private double peso;
	
	pelota(){
	
		radio=100;
		peso =250;
	}
	
	 public pelota(double radio, double peso) {
		this.radio = radio;
		this.peso = peso;
	}


	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	

}
