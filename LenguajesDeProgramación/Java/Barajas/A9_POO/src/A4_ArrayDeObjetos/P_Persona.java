package A4_ArrayDeObjetos;

import java.util.Objects;

import Departamento.Persona;

public class P_Persona {

		private String Nombre;
		private String dni;
		
		
		public P_Persona() {
			
		}
		
		
		
		public P_Persona(String nombre, String dni) {
			Nombre = nombre;
			this.dni = dni;
		}
		
		
		public String getNombre() {
			return Nombre;
		}
		public void setNombre(String nombre) {
			Nombre = nombre;
		}
		public String getDni() {
			return dni;
		}
		public void setDni(String dni) {
			this.dni = dni;
		}






		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			P_Persona other = (P_Persona) obj;
			return Objects.equals(dni, other.dni);
		}
		
		
				

		
}
