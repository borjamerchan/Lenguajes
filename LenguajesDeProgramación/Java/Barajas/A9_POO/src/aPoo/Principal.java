package aPoo;

public class Principal {

	public static void main(String[] args) {
		
		
		Persona P;
		P=new Persona();
		
		P.setDni("25744646");
		
		P.setEdad(21);
		P.getEdad();
		
		P.setNombre("Borja");
		P.getNombre();
		
		P.setDni("324324X");
		P.getDni();
		
		
		P.setAltura(1.78);
		P.getAltura();
		

		System.out.println("Edad -->" + P.getEdad());
		System.out.println("Nombre -->" + P.getNombre());
		System.out.println("Altura -->" + P.getAltura());
		System.out.println("Dni -->" + P.getDni());
		
		// Condicional Con Objeto
		if (P.getEdad() >=18) {
			System.out.print("eres mayor de 18");
		}else {
			System.out.print("eres menor que   18");
		}
	}

	
}
