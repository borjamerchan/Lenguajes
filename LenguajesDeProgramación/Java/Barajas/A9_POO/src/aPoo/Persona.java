package aPoo;

import java.util.Set;

import Ejercicio.Pajaro;

public class Persona {

	private  String Nombre;
	private  int Edad;
	private  String Dni;
	private  double Altura;
	
		
			

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public int getEdad() {
		return Edad;
	}

	public void setEdad(int edad) {
		Edad = edad;
	}

	public String getDni() {
		return Dni;
	}

	public void setDni(String dni) {
		Dni = dni;
	}

	public double getAltura() {
		return Altura;
	}

	public void setAltura(double altura) {
		Altura = altura;
	}

	
}
