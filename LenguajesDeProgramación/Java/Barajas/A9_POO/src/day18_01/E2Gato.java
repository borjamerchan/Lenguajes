package day18_01;

public class E2Gato {
	
	
	String nombre;
	int vidas;
	
	
	public E2Gato(String nombre, int vidas) {
		
		this.nombre = nombre;
		this.vidas = vidas;
	}
	
	
	E2Gato (E2Gato g ){
		this.nombre = g.getNombre();
		this.vidas = g.getVidas();
	}

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getVidas() {
		return vidas;
	}


	public void setVidas(int vidas) {
		this.vidas = vidas;
	}
	
	
	
	

}
