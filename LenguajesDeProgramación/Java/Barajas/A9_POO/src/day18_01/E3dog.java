package day18_01;

public class E3dog {

	
	
		private String name;
		private boolean goodDog = false;
		
		public  E3dog() {
			this.name = "";
			this.goodDog = false;
		}

		public E3dog(String name, boolean goodDog) {
			super();
			this.name = name;
			this.goodDog = goodDog;
		}
	
		
		public boolean isGoodDog() {
			return goodDog;
		}
		
		public void setGoodDog(boolean goodDog) {
			this.goodDog = goodDog;
			
		}
	
		
		public void setName(String name) {
			this.name = name;
			
		}
	
		public String toString() {
			return name + " ,perro " + goodDog;
		}
		
		
		public boolean equels(Object o ) {
			if (this == o) //si tienen la misma referencia
				return true;
			if (o instanceof E3dog) {

			E3dog d =  (E3dog) o;

				if (this.name.equals(d.getName()) && 
					this.goodDog==d.isGoodDog())
					return (true);
			}
		
			return false;
		}

		private Object getName() {
			// TODO Auto-generated method stub
			return null;
		}
		
}
