package day18_01;

public class E1testInicializador {
	
	static {
		System.out.println("Llamada el inicializador");
	}
	
	static {
		
		System.out.println("Llamada el segundo inicializador");
	}
	
	public E1testInicializador() {
		System.out.println("llamada al constructor");
	}
}
