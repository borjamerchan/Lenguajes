package miguel;

public class Ubicacion {
	
	
	
	private   String nombreCalle;
	private   int numeroCalle;
	
	private  SeñalTransito[] señales;
	
	private int numeroDeSeñales;
	
	private final int MAX_SEÑALES;

	public Ubicacion(String nombreCalle, int numeroCalle, int mAX_SEÑALES) {
		this.nombreCalle = nombreCalle;
		this.numeroCalle = numeroCalle;
		MAX_SEÑALES = mAX_SEÑALES;
		this.señales = new SeñalTransito[MAX_SEÑALES];
		this.numeroDeSeñales= 0;
		
	}
	
	public String getnombreCalle() {
	
		return this.nombreCalle;
		
	}
	
	public int getnumeroCalle() {
		
		return this.numeroCalle;
	}
	
	
	public int getnumeroDeSeñales() {
		
		
		return this.numeroDeSeñales;
	}
	
	
	
	
	
	public boolean iguales(Ubicacion ubicacion) {
		
		
		if (this.numeroCalle == ubicacion.numeroCalle && this.nombreCalle.equals( ubicacion.nombreCalle)) {
			
			return true;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
