package Departamento;

public class Persona {

	String nombre;
	String  Dni;
	
	
	Direccion pdireccion;
	Departamento  Departamento;
	
	
	public Persona (String n, String d, Direccion d1) {
		
		this.nombre = n;
		this.Dni = d;
		this.pdireccion = d1;
	}
	public Persona (String n, String d, String c,int num,String l,String p) {
		
		this.nombre = n;
		this.Dni = d;
		this.pdireccion = new Direccion(c,num,l,p);
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDni() {
		return Dni;
	}
	public void setDni(String dni) {
		Dni = dni;
	}
	public Direccion getPdireccion() {
		return pdireccion;
	}
	public void setPdireccion(Direccion pdireccion) {
		this.pdireccion = pdireccion;
	}
	public Departamento getDepartamento() {
		return Departamento;
	}
	public void setDepartamento(Departamento Departamento) {
		this.Departamento = Departamento;
	}
	
	
	
	
	
	
	
	
}
