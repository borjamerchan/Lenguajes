package Departamento;

public class Departamento {

	
	
	
	private String nombre; 
	
	private Persona jefe;

	public Departamento(String nombre, Persona jefe) {
		this.nombre = nombre;
		this.jefe = jefe;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Persona getJefe() {
		return jefe;
	}

	public void setJefe(Persona jefe) {
		this.jefe = jefe;
	}
	
	
}
