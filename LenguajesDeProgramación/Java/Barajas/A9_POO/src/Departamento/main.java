package Departamento;

public class main {

	public static void main(String[] args) {

		
		Direccion d1 = new Direccion("Prudencio Alvaro",51,"Madrid","Madrid");
		Persona p1 = new Persona("Borja", "2343332x",d1);
		Persona p2 = new Persona("pep", "3433433E","Calle To Guapa",23,"Palencia","Palencia2");
		Departamento pd1 = new Departamento("Info", p1);
		Departamento pd2 = new Departamento("Musica", p2);

		p1.setDepartamento(pd1);
		p2.setDepartamento(pd2);
		
		System.out.println("P_Persona 1: " + " Nombre --> " + p1.getNombre()+ ", Dni --> " +p1.getDni()+ ", Calle --> " + p1.getPdireccion().getCalle()+ 
				" Nº" +p1.getPdireccion().getNumero() +" " + p1.getPdireccion().getLocalidad() + " " + p1.getPdireccion().getPovincia());
		System.out.println("P_Persona 2: " + " Nombre --> " + p2.getNombre()+ ", Dni --> " +p2.getDni()+ ", Calle --> " + p2.getPdireccion().getCalle()+ 
				" Nº" +p2.getPdireccion().getNumero() +" " + p2.getPdireccion().getLocalidad() + " " + p2.getPdireccion().getPovincia());

		System.out.println(p1.getDepartamento().getJefe().getNombre() );
		System.out.println(p2.getDepartamento().getJefe().getNombre() );

	}

}
