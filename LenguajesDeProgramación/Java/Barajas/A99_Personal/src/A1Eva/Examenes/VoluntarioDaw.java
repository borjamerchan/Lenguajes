package A1Eva.Examenes;
/**
 * Realiza un programa que pida al usuario un número que indique cuantos números va a introducir (0,5), a continuación lea la cantidad de números indicada (0,5) y devuelva el menor número (0.5) y el mayor número introducido (0,5)
 */
import java.util.Arrays;
import java.util.Scanner;

public class VoluntarioDaw {
    public static void main(String[] args) {
          
        Scanner entrada = new Scanner(System.in);


        int NumeroIntroducidos = 0;

        System.out.println("Introduca la cantidad de números que va a Introducir");

            NumeroIntroducidos = entrada.nextInt();

            int arr[]=new int[NumeroIntroducidos];

          for (int i = 0; i<NumeroIntroducidos; i++){
            System.out.println("Introduzca otro Numero ");
               arr[i]= entrada.nextInt (); //Rellenamos el arreglo.
          }
        
          Arrays.sort(arr); // Ordenar El Array

          int mayor, menor;
          mayor = menor = arr [0];
         
          for (int i = 0; i < arr.length; i++) {
              if(arr [i] > mayor) {
                  mayor = arr[i];
              }
              if(arr[i] < menor) {
                  menor = arr[i];
              }
          }
          System.out.println("El menor valor es: "+menor);
          System.out.println("El mayor valor es: "+mayor);
   }
}
