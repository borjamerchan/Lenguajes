package A1Eva.Examenes;
import java.lang.*;

public class Directorios {

	public static void main(String[] args) {


	      // gets the value of the specified environment variable "PATH"
	      System.out.println("System.getenv");
	      System.out.println(System.getenv("PATH"));

	      // gets the value of the specified environment variable "TEMP"
	      System.out.print("System.getenv");
	      System.out.println(System.getenv("TEMP"));

	      // gets the value of the specified environment variable "USERNAME"
	      System.out.print("System.getenv");
	      System.out.println(System.getenv("USERNAME"));
	}

}
