# Examenes de la Primera Evalación.
- ` (*) Son:`
    - `MODELO A:

1.- (2 puntos) Escribe una función en Java llamada nMúltiplosDeX que reciba un número n y otro x (int ambos) y devuelva un String con los n primeros múltiplos de x separados por comas. Ejemplo nMúltiplosDeX(4,7) deberá devolver “7,14,21,28”

2.- (3 puntos) Dada la siguiente matriz:
char tablero[MAX][MAX]

y siendo MAX una constante entera. Escribe un fragmento de código Java que funcione para cualquier valor de MAX positivo mayor de 2 que rellene la matriz siguiendo el siguiente patrón:

Caso Max impar:`

-`* * *`

 -`.* *.` 

-`* * *`

-`.* *.`

-`* * *`

-`Si no te sale piensa en el caso de max par (no hace falta distinguir par de impar)`

-`* * *`

-`* * *`

-`* * *`

-`* * *`

-`* * *` 

-`* * *`

-`3.- (1,5 puntos) Tienes en la variable fecha de tipo String un texto ya introducido por el usuario. Escribe un fragmento de código Java que compare el texto “01/01/2000” y en caso de coincidir muestre por pantalla “eres el elegido”, en caso contrario valide (escriba correcta o incorrecta) si dicho texto cumple con el siguiente patrón: uno o dos números seguidos por un a barra de dividir (/) luego otra vez uno o dos números seguidos por una barra y por último 2 o 4 números. Ejemplo de posibles fechas válidas: 00/00/00, 40/99/9999, 1/1/9999

4.- (2,5 puntos) Dadas las siguientes definiciones:
char días[] = {'L','M','X','J','V','S','D'};

char mes[] = new char[numDías];

Escribe un fragmento de código Java que rellene el array mes con los caracteres de días, repitiéndolos (al llegar a D pase a L) y empezando la primera posición en la letra indicada por el índice contenido en la variable primerDía. Por ejemplo, si primerDía contiene 3, mes empezará con J, luego V, luego S, luego D, luego L y así sucesivamente.

5.- (1 punto) Estando definido el siguiente tipo enumerado
public enum Día { L,M,X,J,V,S,D}

y la siguientes variable:

Día febrero[] = new Día[29];

posteriormente rellenada con un día diferente en cada posición. Escribe un fragmento de código Java que compruebe si el último día del array febrero se corresponde con lunes (L) sin usar explícitamente el número 31 ni similares (podría ser un mes distinto, con otro número de días) En caso afirmativo basta poner un sysoLn(“Eureka”)


MODELO B:
1.- (3 puntos) Dada la siguiente matriz:

char tablero[MAX][MAX]

y siendo MAX una constante entera. Escribe un fragmento de código Java que funcione para cualquier valor de MAX positivo mayor de 2 que rellene la matriz siguiendo el siguiente patrón: (caso MAX impar, ej: 5)`

-`.* *.`

-`* * *`

 -`.* *.`

-`* * *`

 -`.* *.`

-`Si no te sale piensa en el caso de MAX par (ej: 6, no hace falta distinguir par de impar)`

-`* * *`

-`* * *`

-`* * *`

-`* * *`

-`* * *`

-`* * *`

-`2.- (1,5 puntos) Tienes en la variable uid de tipo String un texto ya introducido por el usuario. Escribe un fragmento de código Java que compare el texto con Neo, en caso afirmativo muestre por pantalla “eres el elegido” , en caso contrario valide (escriba correcto o incorrecto) si el texto cumple con el siguiente patrón: empiece por una letra mayúscula, pueda ir seguido de letras o guiones bajos permitiendo números pero solo al final. Mostrará por pantallla si el uid es válido o no. Ejemplos de uids vaĺidos: X , Xamarin_ , Pepe95, P_e_p_e_5 , PepE, ejemplos de uids no válidos: 67, a7a, _a, a

3.- (2,5 puntos) Dadas las siguientes definiciones:

char días[] = {'L','M','X','J','V','S','D'};

char mes[] = new char[numDías];

Escribe un fragmento de código Java que rellene el array mes con los caracteres de días, repitiéndolos (al llegar a D pase a L) y empezando la primera posición en la letra indicada por el índice contenido en la variable primerDía. Por ejemplo, si primerDía contiene 3, mes empezará con J, luego V, luego S, luego D, luego L y así sucesivamente.


4.- (1 punto) Estando definido el siguiente tipo enumerado

public enum Día { L,M,X,J,V,S,D}

y la siguientes variable:

Día diciembre[] = new Día[31];

posteriormente rellenada con un día diferente en cada posición. Escribe un fragmento de código Java que compruebe si el último día del array diciembre se corresponde con lunes (L) sin usar explícitamente el número 31 ni similares (podría ser un mes distinto, con otro número de días) En caso afirmativo basta poner un sysoLn(“Eureka”)


5.- (2 puntos) Escribe una función en Java llamada nMúltiplosDeX que reciba un número n y otro x (int ambos) y devuelva un String con los n primeros múltiplos de x separados por comas. Ejemplo nMúltiplosDeX(4,7) deberá devolver “7,14,21,28” `