package Array;

import java.util.Scanner;
public class EjemplosDeArray {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        
		int cont;
		
		// Creación de un array de tipo Char:
		
		char MiVectorCaracteres[]=new char[3];
		
		System.out.println("longitud MiVectorCaracteres " + MiVectorCaracteres.length);
		
		// Creación de un array de tipo int:
		
		int[] MiVectorDigitos=new int[5];
		System.out.println("longitud mivectordigitos " + MiVectorDigitos.length);
		
		int mivectordigitos2[]= {3,7,4,4,4,1};
		System.out.println("longitud mivectordigitos2 " + mivectordigitos2.length);
		
		MiVectorCaracteres[0]='a';
		MiVectorDigitos[0]=2;
		
		//rellenar el vectordigitos con numeros del 0 hasta la longitud.
		for (int i=0;i<MiVectorDigitos.length;i++) {
			MiVectorDigitos[i]= i;
		}
		//rellenar el vectorcaracteres con letras que se le piden al usuario
		
		for (int i=0;i<MiVectorCaracteres.length;i++){
			System.out.println("Introduzca una letra: ");
			MiVectorCaracteres[i]=teclado.next().charAt(0);
		}
	
		//mostrar todos los valores del vector de caracteres

		for (cont=0;cont<MiVectorCaracteres.length;cont++){
			System.out.println("El valor de la posición " +cont + "del vector 1 es " + MiVectorCaracteres[cont]);	
		}
		
		//otra forma de recorrer todos los valores.
		
		for (char valor: MiVectorCaracteres)		{
			System.out.println(valor);
				
		}
		
		// Sumar los valores del vectordedigitos	
		int suma=0;
		for (cont=0;cont<MiVectorCaracteres.length;cont++){
			suma=suma + MiVectorDigitos[cont];
				
		}
		System.out.println("El valor de la suma del vector de digitos es  " + suma);
	
		
		
		MiVectorDigitos[0]=8;
		//lo anterior no es copiar es crear un variable que apunte al mismo sitio,
		//luego realmente estoy actuando sobre la misma posici�n de memoria desde dos
		//punteros.
		
		//Para copia hay que ir cargando valor a valor
		int vectorcopiareal[]=new int [MiVectorDigitos.length];

		for (cont=0;cont<MiVectorDigitos.length;cont++){

			vectorcopiareal[cont]=MiVectorDigitos[cont];
			vectorcopiareal[0]=9;

		}
				int otracopiavector[] = new int [MiVectorDigitos.length];
		
		//origen, posicion, destino, posicion, cantidad
		System.arraycopy(MiVectorDigitos, 0, otracopiavector, 0, MiVectorDigitos.length);
    }
}       


