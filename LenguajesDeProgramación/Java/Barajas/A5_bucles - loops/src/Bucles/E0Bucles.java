package Teoria.PrimeraEvalacion.Bucles;

/**
 *0 Escribir por pantalla del 1 al 10
 * 
 *          int num1 = 1 ;
 *      
 *          desde(  numerodeVeces=1 ; numerodeVeces<=10; numeroDeVeces++){
 * 
 *              Escribe (" Numero --> " +  numerodeVeces)
 *          }
 */

 public class E0Bucles {
    public static void main(String[] args) {
   
        
            for ( int i = 1 ; i<=10 ; ++i) {
                System.out.println("Numero --> " + i);
            }
    }
}
