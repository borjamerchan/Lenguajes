package Teoria.PrimeraEvalacion.Bucles;


/*
 * 1 Escribir por pantalla del 0 al 10 de 2 en 2
 * 
 * int num1;
 *  hacer{
 * 
 *  num1 = num1 + 2;
 *  escribe num1;
 * }mientras(0<10);
 * 
 */

public class E1Bucles {
            public static void main(String[] args) {
             
            int num1= 0;

                do{
                    num1 = num1 + 2;
                    System.out.println("numero --> " +  num1);
                }while(num1<10);

            }
}
