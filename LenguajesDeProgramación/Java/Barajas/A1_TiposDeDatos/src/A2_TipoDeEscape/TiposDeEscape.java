package A2_TipoDeEscape;

public class TiposDeEscape {

	public static void main(String[] args) throws Exception {

        /*
         * (*) Tipos de Escape
         *          retorno de carro        \r
         *                 (Nose Que hace xd )
         *          Tabulador Horizontal    \t
         *          Nueva Linea             \n
         *          Barra invertida         \\
         *                 (Poner una \)
         *           Poner Una Comillas \"
         */
            System.out.println("\rEjemplo");
            System.out.println("\t Tabulador");
            System.out.println(" Salto \nDe Linea");
            System.out.println("\\ <-- Insecta una barra");
            System.out.println("Aqui va ir una Comilla --> \" ");
         }
}