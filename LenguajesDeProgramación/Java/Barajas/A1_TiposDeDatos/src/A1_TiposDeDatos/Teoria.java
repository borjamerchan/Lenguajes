	package A1_TiposDeDatos;

public class Teoria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


        /* Guia 
         *
         * (*) tipos Numericos:
         * 
         * Short
         * int Ó Integer  
         * long 
         * double
         * float
         *          ( Para Declarar tienes que poner una F al final del numero --> Como en la linea 43 )

         * (*) Tipos :
         * 
         * char
         *          (Para Declarar un Char tienes que poner las COMILLAS SIMPLES --> '');
         * String 
         *          (Para Declarar un String Tienes que poner COMILLAS DOBLES --> "" );
         * 
         * 
         * (*) Tipos :
         *  
         * boolean 
         *      --> true
         *      --> flase
         */

            System.out.println();
            System.out.println("\tEste Programa Te Va enseñar  a Los Tipos de Datos; ");
            
            System.out.println();
            System.out.println("Los Tipos de Numericos; ");
            System.out.println();

        
            short numeroShort = 2;
            int NumeroInt = 1; 
            long NumeroLong = 12122;
            double NumeroDouble = 121.121;
            float Numerofloat =12.2f;


            System.out.println("\tNumero Short" + numeroShort);
            System.out.println("\tNumero Int " + NumeroInt);
            System.out.println("\tNumero Long " + NumeroLong);
            System.out.println("\tNumero Double" + NumeroDouble);
            System.out.println("\tNumero Float" + Numerofloat);
        
            System.out.println();
            System.out.println("Los Tipos de Texto ");
            System.out.println();
        
    
            char Letra =  'B';
            String Palabra = "Borja";

            System.out.println("\t" + Letra);
            System.out.println("\t" + Palabra);

            System.out.println();
            System.out.println("Los Tipos boolean; ");
            System.out.println();
        
            boolean dato= true;

            System.out.println("\t" + dato);
    }
}
