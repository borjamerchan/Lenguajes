package Ejercicios;

public abstract class Astros {
	
	
	private double RadioeCuatorial;
	private double RotaciónSobreSuEje;
	private double Masa;
	private double Temperatura;
	private double Gravedad;
	
	
	public Astros(double radioeCuatorial, double rotaciónSobreSuEje, double masa, double temperatura, double gravedad) {
		RadioeCuatorial = radioeCuatorial;
		RotaciónSobreSuEje = rotaciónSobreSuEje;
		Masa = masa;
		Temperatura = temperatura;
		Gravedad = gravedad;
	}


	public double getRadioeCuatorial() {
		return RadioeCuatorial;
	}
	public void setRadioeCuatorial(double radioeCuatorial) {
		RadioeCuatorial = radioeCuatorial;
	}
	public double getRotaciónSobreSuEje() {
		return RotaciónSobreSuEje;
	}
	public void setRotaciónSobreSuEje(double rotaciónSobreSuEje) {
		RotaciónSobreSuEje = rotaciónSobreSuEje;
	}
	public double getMasa() {
		return Masa;
	}


	public void setMasa(double masa) {
		Masa = masa;
	}


	public double getTemperatura() {
		return Temperatura;
	}


	public void setTemperatura(double temperatura) {
		Temperatura = temperatura;
	}


	public double getGravedad() {
		return Gravedad;
	}


	public void setGravedad(double gravedad) {
		Gravedad = gravedad;
	}
	
	
	
	public void Muestra() {
		
	}

	
	
	
}
