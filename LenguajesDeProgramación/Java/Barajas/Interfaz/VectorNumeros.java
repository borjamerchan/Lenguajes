

	public class VectorNumeros implements Estadistica {
		  private double[] numeros;
		  
		  public VectorNumeros(double[] numeros) {
		    this.numeros = numeros;
		  }
		  
		  public VectorNumeros(int longitud) {
		    numeros = new double[longitud];
		  }
		  
		  public double minimo() {
		    double min = numeros[0];
		    for (int i = 1; i < numeros.length; i++) {
		      if (numeros[i] < min) {
		        min = numeros[i];
		      }
		    }
		    return min;
		  }
		  
		  public double maximo() {
		    double max = numeros[0];
		    for (int i = 1; i < numeros.length; i++) {
		      if (numeros[i] > max) {
		        max = numeros[i];
		      }
		    }
		    return max;
		  }
		  
		  public double sumatorio() {
		    double suma = 0;
		    for (double n : numeros) {
		      suma += n;
		    }
		    return suma;
		  }
		}

		
	

