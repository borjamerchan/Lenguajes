package Teoria.PrimeraEvalacion.Condiciones;
import java.util.Scanner;


/**
 * 4- Algoritmo que dados dos números, si son pares los suma, y si no dice cual es par y cual impar
		la operación nume%2 si es 0 el número es par
 * 
 * 
 *      int num1, num2, rest;
 * 
 *      leer num1, num2;
 * 
 * 
 *      si (num1 % 2 == 0 and  num2 % 2 == 0) {
 *              rest = num1 +  num2;
 *              escribir (" \"Ha sumado los dos numeros pares\" El resultado -->  " +  rest);
 * 
 *      } sino (num1 % 2 == 1 OR num2 % 2 == 1   )  {
 *               Escribiir ("Un numero es impar No se pueden Sumar\n numero1  --> " + num1 + "Numero2 --> " + num2);
          }
 * 
 */

public class E4Condiciones {
    public static void main(String[] args) {
        Scanner sr = new Scanner(System.in);


        int num1, num2, rest;

            System.out.println("numero1 --> ");
            num1 = sr.nextInt();


            System.out.println("numero2 --> ");
            num2 = sr.nextInt();

            if (num1 % 2 == 0 & num2 % 2 == 0) {
                
                    rest = num1 +  num2;

                    System.out.println(" \"Ha sumado los dos numeros pares\" El resultado -->  " +  rest);

            } else if(num1 % 2 == 1 || num2 % 2 == 1   )  {

                System.out.println("Un numero es impar No se pueden Sumar\n numero1  --> " + num1 + "Numero2 --> " + num2);
            }

    }
    
}
