package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V5_Voluntario5EjerciciosBEntrega23_10;
import java.util.Scanner;

public class E22 {
    public static void main(String[] args) {
       
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca numero de filas: ");
        int numFilas = sc.nextInt();
 
        System.out.println();
        for(int altura = 1; altura<=numFilas; altura++){
            for(int blancos = 1; blancos<=numFilas-altura; blancos++){
                System.out.print(" ");
            }
 
            for(int asteriscos=1; asteriscos<=(altura*2)-1; asteriscos++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
