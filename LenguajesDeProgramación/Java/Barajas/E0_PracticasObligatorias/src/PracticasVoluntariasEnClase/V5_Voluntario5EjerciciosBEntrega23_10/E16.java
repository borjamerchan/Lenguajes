package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V5_Voluntario5EjerciciosBEntrega23_10;
import java.util.Scanner;



/**
 * 16. Realiza un programa en java que pida un número entero positivo y nos diga si es primo o no.
 */

public class E16 {
       public static void main(String[] args) {
        Scanner obtenerNumero = new Scanner(System.in);
        int contador,I,numero;
 
            System.out.print("Ingresa un numero: ");
                numero = obtenerNumero.nextInt();
 
         contador = 0;
 
                if (numero >= 0  ){

                    for(I = 1; I <= numero; I++)
                    {
                        if((numero % I) == 0)
                        {
                            contador++;
                        }
                    }
            
                    if(contador <= 2)
                    {
                        System.out.println("El numero es primo");
                    }else{
                        System.out.println("El numero no es primo");
                    }
                }

    }
}
