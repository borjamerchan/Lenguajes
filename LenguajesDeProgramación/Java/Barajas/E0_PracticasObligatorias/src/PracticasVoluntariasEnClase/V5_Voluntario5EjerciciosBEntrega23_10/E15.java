package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V5_Voluntario5EjerciciosBEntrega23_10;
import java.util.Scanner;

/**
 * 15. Realiza un programa que cuente los múltiplos de 3 desde el 1 hasta un número que introducimos por teclado.
 */

public class E15 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        int num , cantidad;

        System.out.println("Dime Un numero");
            num = entrada.nextInt();

        while(num % 3 == 0){

            cantidad =    num / 4;
            cantidad = cantidad +1;

            System.out.println("Es Multiplo de 3:  " + num);
            System.out.println("cantidad de Multipo "  +  cantidad);
            
            break;
       }
    }
}
