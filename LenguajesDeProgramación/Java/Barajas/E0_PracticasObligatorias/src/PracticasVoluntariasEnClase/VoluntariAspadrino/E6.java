package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.VoluntariAspadrino;
import java.util.Scanner;

/**
 * 6. Realiza un programa que lea 10 números no nulos y luego muestre un mensaje de si ha leído algún número negativo o no.
 */

public class E6 {
    public static void main(String[] args) {
        Scanner sr = new Scanner(System.in);

        int numeros,cont = 0;
        int ContNegativo= 0;

        while ( cont <10 ){

                cont++;

            System.out.println("Dime numeros");
                numeros = sr.nextInt();

            if (numeros < 0 ){

                    ContNegativo++;
                    
            }
        }
        System.out.println("Has Introducido estos Numero Negativos --> " + ContNegativo);
    }
}
