package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.VoluntariAspadrino;

/**
 * 3.Realiza un programa que muestre los números pares comprendidos entre el 1 y el 200. Esta vez utiliza un contador sumando de 1 en 1. 
 */

public class E3 {
    public static void main(String[] args) {

        int numero = 0;
        
        while(numero < 200){
            numero++;

                if (numero%2==0 ){
                    System.out.println("Par -->" + numero);


                }else{

                 //System.out.println(" Impar --> " + numero);

                }
        }
    }
}
