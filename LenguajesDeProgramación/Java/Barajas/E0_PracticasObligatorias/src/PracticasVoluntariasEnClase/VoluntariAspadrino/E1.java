package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.VoluntariAspadrino;

/**
 * 1. Realiza un programa que muestre por pantalla los 20 primeros números naturales (1, 2, 3... 20).
 */
 
public class E1 {
    public static void main(String[] args) {
        
        int num = 0;

        while(num < 20){

            num++;
            System.out.println(num +  "<--Numero");
            
        }
    }
}
