package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.VoluntariAspadrino;
import java.util.Scanner;


/**
 * 10. Dibuja un ordinograma de un programa que lee una secuencia de notas (con valores que van de 0 a 10) 
 * que termina con el valor -1 y nos dice si hubo o no alguna nota con valor 10.
*/
public class E10 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        int numNegativo = -1;
        int numeros = 0,cont = 0;

        boolean hayValor = false;

        while(numeros < 10  ){

            System.out.println("Introduce un Numero ");
            numeros = entrada.nextInt();

            if (numeros == numNegativo){
                System.out.println("Fin Es numero Negativo");
                break;
                
            }
        }

        if (numeros == 10 ){

            hayValor = true;
            System.out.println("Se han Introducido un 10");

        }else{

            hayValor = false;
            System.out.println("No se ha Introducido 10");

        }
    }
}
