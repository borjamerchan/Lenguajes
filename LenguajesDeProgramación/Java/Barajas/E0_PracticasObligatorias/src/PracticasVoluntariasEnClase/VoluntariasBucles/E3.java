package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.VoluntariasBucles;
import java.util.Scanner;

public class E3 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        int num1,num2;

        System.out.println("Dame Un Numero");
        num1 = entrada.nextInt();

        System.out.println("Dame Un Numero2");
        num2 = entrada.nextInt();
        
        while (num1<num2-1){

            ++num1;
            System.out.println("Numero " + num1);

        }
    }
}
