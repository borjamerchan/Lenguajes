package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V3_Voluntarios3Condicionales;
import java.util.Scanner;
public class E3 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        // Con Numero
        int num1;
        int   num2;

        System.out.println("Dame Un Numero1");
        num1 = entrada.nextInt();
        
        System.out.println("Dame Un Numero2");
        num2 = entrada.nextInt();

        if (num1 > num2){
            System.out.println("El numero 1 Es mayor");
        }else{
            System.out.println("El Numero 2 Es mayor");
        }
    }
}
