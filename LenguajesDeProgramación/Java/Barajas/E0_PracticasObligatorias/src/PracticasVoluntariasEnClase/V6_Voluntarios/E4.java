package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V6_Voluntarios;
import java.util.Scanner;

/*
EC6 Programa java que lea un carácter por teclado y compruebe si es un dígito numérico (cifra
entre 0 y 9)
Pistas: Leer de otra forma a la clase Scanner, hacer uso de la clase Character
 */

public class E4 {
    public static void main(String[] args) {
        Scanner sr = new Scanner(System.in);

        System.out.print("Introduzca carácter: ");
            char num = sr.next().charAt(0);
        
            if(num>='0' && num<='9'){
            System.out.println("Es un número");
            }else{
            System.out.println("No es un número");  
            }
            
    }
}
