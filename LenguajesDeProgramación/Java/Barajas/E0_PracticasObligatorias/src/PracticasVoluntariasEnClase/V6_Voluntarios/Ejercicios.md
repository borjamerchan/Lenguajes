BASICOS

B10 Programa Java que lea un número entero de 3 cifras y muestre por separado las cifras del número, indicando
cual es la primera cifra, la cifra central y la última cifra
Ejemplo número 357
primera cifra 3
cifra central 5
última cifra 7

B13 Programa que pida por teclado la fecha de nacimiento de una persona (dia, mes, año) y calcule su número
de la suerte.
El número de la suerte se calcula sumando el día, mes y año de la fecha de nacimiento y a continuación
sumando las cifras obtenidas en la suma.
Por ejemplo:
Si la fecha de nacimiento es 12/07/1980
Calculamos el número de la suerte así: 12+7+1980 = 1999 1+9+9+9 = 28
Número de la suerte: 28

ESTRUCTURAS CONDICIONALES 

EC3 Programa que lea un carácter por teclado y compruebe si es una letra mayúscula. 
Pistas: Leer de otra forma a la clase Scanner, hacer uso de la clase Character

EC6 Programa java que lea un carácter por teclado y compruebe si es un dígito numérico (cifra
entre 0 y 9)
Pistas: Leer de otra forma a la clase Scanner, hacer uso de la clase Character

EC10 Programa que lea una variable entera mes y muestra por pantalla el nombre del mes y los días que tiene (30. 31 o 28). Se debe
comprobar que el valor introducido esté comprendido entre 1 y 12

ESTRUCTURAS REPETITIVAS

ER6 Utilizando la sentencia repetitiva do...while realizar un programa en el que se ingresan un conjunto de n edades de personas por teclado. El programa finalizara cuando el promedio de las edades sea superior a 25.


ER7 Utilizando la sentencia for confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar del mismo (los primeros 12 términos) siguiendo el modelo que se muestra en el ejemplo.
Ejemplo: Si ingreso 3 deberá aparecer en pantalla lo siguiente
3 x 1 = 3
3 x 2 = 6
3 x 3 = 9
3 x 4 = 12
3 x 5 = 15
3 x 6 = 18
3 x 7 = 21
3 x 8 = 24
3 x 9 = 27
3 x 10 = 30
3 x 11 = 33
3 x 12 = 36
