package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V6_Voluntarios;
import java.util.Scanner;

/**
 * 
 * ER7 Utilizando la sentencia for confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar del mismo (los primeros 12 términos) siguiendo el modelo que se muestra en el ejemplo.

 Ejemplo: Si ingreso 3 deberá aparecer en pantalla lo siguiente
            3 x 1 = 3
            3 x 2 = 6
            3 x 3 = 9
            3 x 4 = 12
            3 x 5 = 15
            3 x 6 = 18
            3 x 7 = 21
            3 x 8 = 24
            3 x 9 = 27
            3 x 10 = 30
            3 x 11 = 33
            3 x 12 = 36

 */

public class E7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = 0;

        System.out.print("Introduce un número entero: ");                                                         
            n = sc.nextInt();

        System.out.println("Tabla del " + n);

        for(int i = 1; i<=10; i++){

             System.out.println(n + " * " + i + " = " + n*i);

        }
    }
}
