package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V6_Voluntarios;
import java.util.Scanner;


/**
 * B10 Programa Java que lea un número entero de 3 cifras y muestre por separado las cifras del número, indicando cual es la primera cifra, la cifra central y la última cifra
Ejemplo número 357
primera cifra 3
cifra central 5
última cifra 7

 */
public class E1 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int N;

        System.out.print("Introduzca valor de N: ");
            N = sc.nextInt();   
        
            System.out.println("Primera cifra de " + N + " -> " + (N/100));
            System.out.println("Cifra central de " + N + " -> " + (N/10)%10);
            System.out.println("Última cifra  de " + N + " -> " + (N%10));
        }
    }
