package Practica.DiagramasDeFlujo.PracticasVoluntariasEnClase.V6_Voluntarios;
import java.util.Scanner;
/**
 * 
 * Programa que pida por teclado la fecha de nacimiento de una persona (dia, mes, año) y calcule su número de la suerte. El número de la suerte se calcula sumando el día, mes y año de la fecha de nacimiento y a continuación
sumando las cifras obtenidas en la suma.
Por ejemplo:
Si la fecha de nacimiento es 12/07/1980
Calculamos el número de la suerte así: 12+7+1980 = 1999 1+9+9+9 = 28
Número de la suerte: 28
 */

public class E2 {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int dia, mes, año, suerte, suma, cifra1, cifra2, cifra3, cifra4;                                          

        System.out.println("Introduce una tu  fecha de nacimiento");
        
        System.out.print("día: ");
            dia = sc.nextInt();
        System.out.print("mes: ");
            mes = sc.nextInt();
        System.out.print("año: ");
            año = sc.nextInt();

            suma = dia + mes + año;

                cifra1 = suma/1000;      //obtiene la primera cifra
                cifra2 = suma/100%10;    //obtiene la segunda cifra
                cifra3 = suma/10%10;     //obtiene la tercera cifra
                cifra4 = suma%10;        //obtiene la última cifra
                suerte = cifra1 + cifra2 + cifra3 + cifra4;

            System.out.println("Su número de la suerte es: " + suerte);    
        
        /**
         *     Scanner sc= new Scanner (System.in);

                System.out.println("año");
                int a=sc.nextInt();
                System.out.println("mes ");
                int m=sc.nextInt();
                System.out.println("dia");
                int d=sc.nextInt();
                int s=a+m+d;

                String v=Integer.toString(s);
                int sum=0;
                for (int i = 0; i <v.length(); i++) {
                sum=sum+Integer.parseInt(v.charAt(i)+"");

                }
                    System.out.println("nUMERO DE LA SUERTE "+sum);
         */
    }
     
}
