package DAMP5ArrayListProductos;

import java.util.ArrayList;
import java.util.Iterator;


public class main {

	public static void main(String[] args) {

		
		
		//	Crea 5 instancias de la Clase Producto.

		Producto p1 = new Producto("Sandias",3);
		Producto p2 = new Producto("Platano",2);
		Producto p3 = new Producto("uvas",1);
		Producto p4 = new Producto("Pera",3);
		Producto p5 = new Producto("melocotón",2);

		//	Crea un ArrayList.
		

		
		
		  ArrayList<Producto> productos = new ArrayList<Producto>();
		  	
		  	//	Añade las 5 instancias de Producto al ArrayList.
				
		        productos.add(p1);
		        productos.add(p2);
		        productos.add(p3);
		        productos.add(p4);
		        productos.add(p5);
		        
				//	Visualiza el contenido de ArrayList utilizando Iterator.

	        System.out.println("Visualiza el contenido");

	        Iterator<Producto> iterator = productos.iterator();

	        while (iterator.hasNext()) {
	            Producto producto = iterator.next();
	            System.out.println("Nombre: " + producto.getNombre() + "  La Cantidad es  de --> " + producto.getCantidad());
	        }

			//	Elimina dos elemento del ArrayList.
	        
	        productos.remove(1);
	        productos.remove(2);

	        
	        
	        
	        /**
	         * 
	         * Preguntar como se busca el nombre del la fruta otra vez?
	         */
	        
	        //Inserta un nuevo objeto producto en medio de Ia lista.

	        productos.add(2, new Producto("Fresas", 10));


			//	 de nuevo el contenido de ArrayList utilizando Iterator.

	        System.out.println("Visualizado el contenido otraa vez");

	        iterator = productos.iterator();

	        while (iterator.hasNext()) {
	            Producto producto = iterator.next();
	            System.out.println("Nombre De la fruta: " + producto.getNombre() + "  La Cantidad es  de -->  " + producto.getCantidad());

	        }


			//	Elimina todos los valores del ArrayList.

	        productos.clear();

	        System.out.println(" Eliminado las frutas" + productos);
	}

	
}


