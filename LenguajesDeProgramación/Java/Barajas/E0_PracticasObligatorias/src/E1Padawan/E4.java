import java.util.Scanner;

/**
 * 4. Dibuja un ordinograma que lea dos números, calcule y muestre el valor de sus suma, resta, producto y división.
 */

public class E4 {
    public static void main(String[] args) throws Exception {

        Scanner entrada = new Scanner(System.in);
        int números1;
        int números2;
        int ResultadoSuma;
        int Resultadoresta;
        int ResultadorMultiplicación;
        double ResultadoDivisión;
        double ResultadoProducto;

        System.out.println("Introduce un numero");
        números1  = entrada.nextInt();

        System.out.println("Introduce El Segundo Numero");
        números2 = entrada.nextInt();

        ResultadoSuma = números1 + números2;
        Resultadoresta = números1 - números2;
        ResultadorMultiplicación = números1 * números2;
        ResultadoDivisión =  números1 / números2;
        ResultadoProducto = números1 % números2;

        System.out.println("Suma: " + ResultadoSuma );
        System.out.println("Resta: " + Resultadoresta );
        System.out.println("Multiplicación: " + ResultadorMultiplicación);
        System.out.println("División: " + ResultadoDivisión);
        System.out.println("Producto: " + ResultadoProducto );
        
    }//main
}//Class
