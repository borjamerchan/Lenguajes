

/**
 * 2. Dibuja un ordinograma que calcule y muestre el área de un cuadrado de lado igual a 5.
 */

public class E2 {
public static void main(String[] args) {
    
         short  Cuandrado = 22;
         int CadaLado;
         int Area;


         CadaLado = Cuandrado / 4 ;
         Area = CadaLado * CadaLado;

         System.out.println("Cada Lado del cuadrado --> " + CadaLado);
         System.out.println("La Area del Cuadrado --> " + Area);
        }
}
