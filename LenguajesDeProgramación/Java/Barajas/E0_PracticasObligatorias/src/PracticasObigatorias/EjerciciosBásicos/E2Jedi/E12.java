package Practica.DiagramasDeFlujo.PracticasObigatorias.EjerciciosBásicos.E2Jedi;
import java.util.Scanner;

/*
 * 12. Dibuja el ordinograma de un programa que lee un número y me dice si es positivo o negativo, consideraremos el cero como positivo.
 */

public class E12 {
    public static void main(String[] args) {

        Scanner sr = new Scanner(System.in);

        int numero;

        System.out.println("dime un numero");
            numero = sr.nextInt();

        if (numero >= 0){
            System.out.println("Es positivo");
        }else{
            System.out.println("Es Negativo");
        }
    }
}
