package Practica.DiagramasDeFlujo.PracticasObigatorias.EjerciciosBásicos.E2Jedi;
import java.util.Scanner;


 /*
 * Dibuja un ordinograma que lee dos números, calcula y muestra el valor de su suma, resta,producto y división. (Ten en cuenta la división por cero ).
 */

 public class E10 {
    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        int numero1, numero2;
        int suma ,resta ,multi, division;

        System.out.println("\t Indica El numero1");
        numero1 = entrada.nextInt();

        System.out.println("\t Indica El numero2");
        numero2 = entrada.nextInt();

            if (numero2 != 0){
                
                suma = numero1 + numero2;
                resta = numero1 - numero2;
                multi = numero1 * numero2;
                division = numero1 / numero2;

                System.out.println("\nSuma --> " + suma + "\n Resta --> " + resta +"\n Multiplicación --> " + multi + "\n División --> "+ division);

            }else{

                System.out.println("No Se puede Divir entre \" Sí se Divide por 0  Te parezca un Error\" "  );
                suma = numero1 + numero2;
                resta = numero1 - numero2;
                multi = numero1 * numero2;

                System.out.println("Suma --> " + suma + "\n Resta --> " + resta +"\n Multiplicación --> " + multi );

            }
            
    }    
}
