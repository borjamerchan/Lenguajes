package Practica.DiagramasDeFlujo.PracticasObigatorias.EjerciciosBásicos;
import java.util.Scanner;

/**
 * 13. Dibuja un ordinograma de un programa donde el usuario "piensa" un número del 1 al 100 y el ordenador intenta adivinarlo
 * Es decir, el ordenador irá proponiendo números una y otra vez hasta adivinarlo 
 * (el usuario deberá indicarle al ordenador si es mayor, menor o igual al número que ha pensado).
*/

public class E13Plus {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        int num = 0; 
        int numeroAleatorio = 0;
        numeroAleatorio = (int)(Math.random()*100);

            while( num <100){

                System.out.println("dame un numero");
                num = entrada.nextInt();

                    if ( num == numeroAleatorio){
                        
                        System.out.println("Lo has Adivinado");
                        break;

                    }//if
                    if ( num < numeroAleatorio){
                        
                        System.out.println("Tienes que Poner un numero mas Mayor ");

                    }else {

                        System.out.println("Tienes que Poner un numero mas Menor " );

                    }//else
            }//while
    }//main 
}//class