package Practica.DiagramasDeFlujo.PracticasObigatorias.EjerciciosBásicos.E1Padawan;
import java.util.Scanner;

/*
 * 3. Dibuja un ordinograma que calcule el área de un cuadrado cuyo lado se introduce por teclado.
 */

 public class E3 {
    public static void main(String[] args) {
    
        Scanner entrada = new Scanner(System.in);

        int Area;
        int Cuadrado;
        int CadaLado ;

        System.out.println("Dime la longitud del Cuandrado");
        Cuadrado = entrada.nextInt();

        CadaLado = Cuadrado / 4 ;
        Area = CadaLado * CadaLado;

        System.out.println("Cada lado es de --> " + CadaLado);
        System.out.println("la Area es de --> " +  Area);
    }
}
