package Practica.DiagramasDeFlujo.PracticasObigatorias.EjerciciosBásicos.E1Padawan;
import java.util.Scanner;

/**
 * 7. Dibuja un ordinograma que lea un valor correspondiente a una distancia en millas marinas y escriba la distancia en metros. Sabiendo que una milla marina equivale a 1.852 metros
 */

public class E7 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        int millas = 1852;
        int MillasMarinas;
        int Convertir;


        System.out.println("¿Cuantas Millas Marinas a Metros ?");
        MillasMarinas = entrada.nextInt();

        Convertir =  millas * MillasMarinas;

        System.out.println(" Millas Marinas A  --> " + Convertir + "Metros" );
    }
}
