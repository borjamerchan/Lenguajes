package Practica.DiagramasDeFlujo.PracticasObigatorias.EjerciciosBásicos.E1Padawan;
import java.util.Scanner;

/*
 * 8. Dibuja un ordinograma de un programa que pide la edad por teclado y nos muestra el mensaje de “Eres mayor de edad” solo si lo somos.
 */

public class E8 {
    public static void main(String[] args) {
    Scanner entrada = new Scanner(System.in);

        int edad;

        System.out.println("Cuantos Años Tienes");
        edad = entrada.nextInt();

        if (edad >=18){
            System.out.println("Eres mayor edad");
        }
    }
}
