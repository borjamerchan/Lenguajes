package Practica.DiagramasDeFlujo.PracticasObigatorias.EjerciciosBásicos;
import java.util.Scanner;

import javax.swing.text.StyledEditorKit.BoldAction;

/**
 * 10. Dibuja un ordinograma de un programa que lee una secuencia de notas (con valores que van de 0 a 10) 
 * que termina con el valor -1 y nos dice si hubo o no alguna nota con valor 10.
*/
public class E10 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        int num = 0;    
        int numNegativo = -1; 

        boolean Numero10 = false; 


        while(num < 10  ){

            if (num == numNegativo){
                System.out.println("Fin Es numero Negativo");
                break;
                
            } else if (num == 10){

                Numero10 = true;


            }

            

            System.out.println("Introduce un Numero ");
            num = entrada.nextInt();

        }

        if (Numero10 == true){

            System.out.println("hay al menos una nota de 1o");
        }else{

            System.out.println("No Hubo Notas De 10 ");
        }
    }
}
