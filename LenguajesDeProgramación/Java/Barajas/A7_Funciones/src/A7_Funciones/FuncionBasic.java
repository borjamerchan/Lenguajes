package A7_Funciones;

import java.util.Arrays;

public class FuncionBasic {

	public static void suma_x_al_vector(int v[], int x) {
		for (int i = 0; i< v.length; i++)
		v[i] = v[i] + x;
		}
		
	
	
		public static void main(String[] args) {
			int v[] = {0, 1, 2, 3};
			System.out.println("Vector antes: " + Arrays.toString(v));
			suma_x_al_vector(v, 10);
			System.out.println("Vector después: " + Arrays.toString(v));
		}
}
