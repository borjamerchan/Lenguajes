import java.util.Scanner;

public class EntradaYSalidaDeDatos {
	public static void main(String[] args) throws Exception {

        /*
         * Entrada y Salida de datos:
         * 
         * (*) inportamos la liberia de Scanner
         * 
         * (*) import java.util.Scanner;
         * (*) Tinenes que poner --> Scanner //entrada = new Scanner(System.in);
         * (*) Pones lo que el usuario quiera guardar --> System.out.println("Texto");
         * (*) Depende que tipo de dato Sea pones lo siguiente:
         *      (*) Si es de tipo String --> String texto = entrada.nextline();
         *      (*) Si es de Tipo int --> int numero = entrada.nextInt();
         *         
         */

        Scanner entrada = new Scanner(System.in);

        int age;
        String name;

        System.out.println("Introduce Tu nombre ");
        name = entrada.nextLine();

        System.out.println("Dime tu edad");

        age = entrada.nextInt();

        System.out.println("nombre: " + name + "\nEdad: " + age);
    }
}