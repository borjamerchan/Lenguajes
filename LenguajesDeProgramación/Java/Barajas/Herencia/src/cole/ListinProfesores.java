package cole;

import java.util.ArrayList;
import java.util.Iterator;


public class ListinProfesores {

	private static final float Of = 0;
	private ArrayList <Profesor> listinProfesores; //Campo de la clase

	public ListinProfesores () {

		listinProfesores = new ArrayList <Profesor> ();
	} //Constructor

	public void addProfesor (Profesor profesor) {
		listinProfesores.add(profesor);
	}
	

	public void imprimirListin() { 
		String tmpStrl = ""; //String temporal gue usamos como auxiliar
		System.out.println ("Se procede a mostrar los datos de los profesores existentes en el listin \n");
		
		for (Profesor tmp: listinProfesores) {
			System.out.println (tmp.toString () );
			
		if (tmp instanceof ProfesorInterino) {
			tmpStrl = "Interino";
		}
		else {
			tmpStrl = "Titular";
		}
		
		System.out.println("-Tipo de este profesor:"+tmpStrl +" -Noémina de este profesor: "+(tmp.importeNomina())+ "\n");
		}	
		}
		
		public float importeTotalNominaProfesorado() {
			float importeTotal = Of; //Variable temporal gue usamos como auxiliar
			
			Iterator<Profesor> it = listinProfesores.iterator();
				
			while (it.hasNext() ) {
				importeTotal = importeTotal + it.next() .importeNomina();
			}
			return importeTotal;

		 //Cierre del método importeTotalNominaProfesorado
			
		}
}
