package cole;

import java.util.Calendar;

public class TestAbtract {

	public static void main(String[] args) {

		Calendar fechal = Calendar.getInstance();
		fechal.set(2019,10,22); //Los meses van de 0 a 11, luego 10 representa noviembre
		ProfesorInterino pi1 = new ProfesorInterino("José", "Hernandez Lépez", 45, "45221887-K", fechal);
		ProfesorInterino pi2 = new ProfesorInterino("Andrés", "Molté Parra", 87, "72332634-L", fechal);
		ProfesorInterino pi3 = new ProfesorInterino ("José", "Rios Mesa", 76, "34998128-M", fechal);
		ProfesorTitular ptl = new ProfesorTitular ("Juan", "Pérez Pérez", 23, "73-K");
		ProfesorTitular pt2 = new ProfesorTitular ("Alberto", "Centa Mota", 49, "88-L");
		ProfesorTitular pt3 = new ProfesorTitular ("Alberto", "Centa Mota", 49, "81-F");		 

		ListinProfesores listinProfesorado = new ListinProfesores ();
		listinProfesorado.addProfesor (pi1);
		listinProfesorado.addProfesor (pi2);
		listinProfesorado.addProfesor (pi3);
		listinProfesorado.addProfesor (ptl);
		listinProfesorado.addProfesor (pt2);
		listinProfesorado.addProfesor (pt3);
		listinProfesorado.imprimirListin();

		System.out.println ("El importe de las néminas del profesorado que consta en el listin es " +
		listinProfesorado.importeTotalNominaProfesorado()+ " euros");
		

	}
}
