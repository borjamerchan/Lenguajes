package modelodatos;

public class Profesor {

	protected String nombre;
    protected int edad;
    protected int añosConsolidados;
    
    public Profesor(String nombre, int edad, int añosConsolidados) {
        this.nombre = nombre;
        this.edad = edad;
        this.añosConsolidados = añosConsolidados;
    }
    
    public double obtenerSalarioBase() {
        return 925 + añosConsolidados * 33.25;
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getAñosConsolidados() {
		return añosConsolidados;
	}

	public void setAñosConsolidados(int añosConsolidados) {
		this.añosConsolidados = añosConsolidados;
	}
	
	
	
	
}
