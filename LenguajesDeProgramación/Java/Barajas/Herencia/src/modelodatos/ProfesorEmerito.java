package modelodatos;

public class ProfesorEmerito  extends Profesor {
	
	
	

	private int añosEmerito;
    
	
	
    public ProfesorEmerito(String nombre, int edad, int añosConsolidados, int añosEmerito) {
        super(nombre, edad, añosConsolidados);
        this.añosEmerito = añosEmerito;
    }
    
    public int getAñosEmerito() {
		return añosEmerito;
	}

	public void setAñosEmerito(int añosEmerito) {
		this.añosEmerito = añosEmerito;  
	}
    private double obtenerSalarioBaseSinSuper() {
        return 925 + añosConsolidados * 33.25 + 47.80 * añosEmerito;
    }
    
    
    
    @Override
    public double obtenerSalarioBase() {
        return super.obtenerSalarioBase() + 47.80 * añosEmerito;
    }
}
