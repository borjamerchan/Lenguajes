package herencia;


	
	class Triangle extends TwoDShape {

		String style;
		
		public String getStyle() {
			return style;
		}
		public void setStyle(String style) {
			this.style = style;
		}
		double area() {
			return width * height / 2;
		
		}
		void showStyle() {
			System. out.println("Triangle is " + style);
		
		}
	}

