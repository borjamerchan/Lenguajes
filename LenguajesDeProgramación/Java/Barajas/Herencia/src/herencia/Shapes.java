package herencia;

public class Shapes {

	public static void main(String[] args) {
		
		Triangle tl = new Triangle();
		Triangle t2 = new Triangle();

		tl.setWidth(4.0); 
		tl.setHeight(4.0);
		tl.setStyle("filled");
		t2.setWidth(8.0); 
		t2.setHeight(12.0); 
		t2.setStyle("outlined");  
		System. out.println ("Info for t1");
		tl.showStyle();
		tl.showDim();

		System. out.print("Area is " + tl.area());
		System. out.println();
		System. out.println ("Info for t2: ");
		t2.showStyle();
		t2.showDim();

		System. out.print ("Area is " + t2.area());
		
	}

}
