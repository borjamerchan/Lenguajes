package vista;

import modelodatos.Profesor;
import modelodatos.ProfesorEmerito;

public class Principal {

	public static void main(String[] args) {
		
		        Profesor profesor = new Profesor("Borja", 90, 20);
		        ProfesorEmerito profesorEmérito = new ProfesorEmerito("pepe", 120, 10, 1);
		        
		        System.out.println("Salario de " + profesor.getNombre() + ": " + profesor.obtenerSalarioBase());
		        System.out.println("Salario de " + profesorEmérito.getNombre() + ": " + profesorEmérito.obtenerSalarioBase());
		    }
	}


