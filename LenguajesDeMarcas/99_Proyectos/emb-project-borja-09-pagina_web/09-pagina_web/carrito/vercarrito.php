<?php
//Creamos sesión y una conexión con la base de datos.
ob_start("ob_gzhandler");
session_start();
extract($_REQUEST);

$servidor = "localhost";
$BD = "web";
$nombre = "root";
$contrasena = "";

// Create connection
$conexion = mysqli_connect($servidor, $nombre, $contrasena, $BD);
// Comprobar la conexión
if (!$conexion) {
    die("Ha fallado la conexión: " . mysqli_connect_error());
}


//Comprobamos que la variable $carro tenga valor.
if(isset($_SESSION['carro'])){
  $carro=$_SESSION['carro'];
}
else {
  $carro=false;
}

//Hacemos la consulta a la base de datos para mostrar los productos
$consulta=("SELECT * FROM Producto WHERE idProducto=".$id.";");
$resultado = mysqli_query($conexion, $consulta);

?>

<?php
    $usuario = $_GET['u'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <!-- Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <meta charset="UTF-8">
    <title>BIBLIOTECA EMB&CO</title>
    <link type="text/css" rel="stylesheet" href="css/pie.css">
    <link type="text/css" rel="stylesheet" href="css/Menu.css">
    <link type="text/css" rel="stylesheet" href="css/cuerpo.css">
    <link rel="stylesheet" href="css/estilo_cajas.css">
    <link type="text/css" rel="stylesheet" href="css/general.css">


  </head>
  <body>
<!--///////////////////////////////////////////////////////////////////////////////////////////-->

   
    
<header>
        <center>
          <div class="ancho">
        <div class="logo">
          <p> <br>EMB  & CO  <img src="../content/LGOpng.png" alt="Girl in a jacket" width="100" height="100">        </div> </p>
        </div>
            <nav class="navegar">
                <ul class="menu">
                    <li><a href="../index.php?u=<?php echo $usuario ?>"> Home </a></li>   
                    <li> <a href="#">Libros </a>
                         <ul class="submenu">
                            <li><a href="#"> Categoria </a></li>
                            <li><a href="#"> Tipo de libros </a></li>
                           
                        </ul>
                    </li>
                   
                    <li><a href="catalogo.php?u=<?php echo $usuario ?>"> Prestamo </a></li>   
                    <li><a href="../formulario.php?u=<?php echo $usuario ?>"> ¿Buscas un Libro? </a>
                       
                    </li>
                <li> 
                  
                <a class="list-group-item" href="../cerrar_login.php"><i class="fa fa-user" aria-hidden="true"User:?php echo $usuario ?> <?php echo $usuario ?></i>&nbsp; </a>
                
              
                    <li> <a class="list-group-item" href="#"><i class="fa fa-cog fa-fw" aria-hidden="true"></i>&nbsp; </a>
                        <ul class="submenu">
                            <li><a href="#"> Cuenta </a></li>

                </ul>

            </nav>
            
        </center>
    </header>
    <div id="linea"></div>

  
<!--///////////////////////////////////////////////////////////////////////////////////////////-->
<center>
  <br>
 <?php if($carro){ // Si $carro tiene algo lo muestra en la tabla. ?>

<table>
    <tr>
      <td width="9%">CANTIDAD</td>

      <td width="48%">NOMBRE</td>
      <td width="19%">PRECIO</td>
      <td width="10%">BORRAR</td>
    </tr>

    <?php 

      $color=array("#33ddff","#33ddff");
      $contador=0;
      $suma=0;
      

      foreach($carro as $k => $v){
         $row = mysqli_fetch_assoc($resultado);
         $subto=$v['cantidad']*$v['Precio'];
         $suma=$suma+$subto;
         $contador++; 
      ?>

<form name="a<?php echo $v['identificador'] ?>" method="post" action="
agregacar.php?<?php echo SID ?>&id=<?php echo $row['id']?>"
id="a<?php echo $v['identificador'] ?>">
      <tr bgcolor="<?php echo $color[$contador%2]; ?>">
        <td>
          <?php 
          echo $v['cantidad'];  ?>
        </td>
      
        <td>
            <?php echo $v['Nombre'] ?></a>
        </td>
        <td>
            <?php echo $v['Precio'] ?> €<br />
        </td>
        <td>
              <a href="borracar.php?<?php echo SID ?>&id=<?php echo $v['id'] ?>&precio=<?php echo $precio ?>"><img src="img/btBorrar.gif" alt="Borrar
             articulo" width="20" height="20" /></a>
        </td>
     </tr>
   </form>
     <tr>
<?php } //fin foreach ?>

      <td colspan="5"><br><p>TOTAL  PRODUCTOS:
      <?php echo count($carro); ?><br />
      TOTAL PRECIO: <?php echo number_format($suma,2); ?> &euro;</p></td>
    </tr>
</table>
<div id="boton"><a href="catalogo.php?<?php echo SID?>">CONTINUAR COMPRA</a></div>
<div id="boton"><a href="pedido.php?<?php echo SID?>&precio=<?php echo $precio ?>">FINALIZAR COMPRA</a></div>
<?php }else{ // // Si $carro NO tiene nada solo muestra un link a index.php.?>
<p>El carrito de compra está vacío.</p>
<div id="boton"><a href="catalogo.php<?php echo SID?>">CONTINUAR COMPRA</a></div>

<?php }?>


<?php //Al final del archivo liberamos recursos

ob_end_flush();
mysqli_free_result($resultado);
mysqli_close($conexion); 

?>

</body>
</html>
