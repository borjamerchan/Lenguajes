<?php
    $usuario = $_GET['u'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sesion iniciada</title>
    
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    
    <link rel="stylesheet" href="css/login.css">
    
    <style>
        
        body{
            background-image: url(image/bg3.jpg);
        }
        
        .welcome{
            width: 100%;
            max-width: 800px;
            margin: auto;
            margin-top: 20px;
            background: rgba(0,0,0,0.6);
            text-align: center;
            padding: 20px;
        }
        
        .welcome img{
            width: 500px;
            height: 120px;
            text-align: center;
        }

        .welcome h1{
            font-size: 40px;
            color: white;
            font-weight: 100;
            margin-top: 20px;
        }
        
        .welcome a{
            display: block;
            margin-top: 10px;
            font-size: 30px;
            padding: 2px;
            border: 1px solid blue;
        }
        
        .welcome a:hover{
            color: black;
            background: white;
        }
        
    
    </style>
    
</head>
<body>
   
   <div class="welcome">
       <img src="image/logodosa.png" >
        <h1>Bienvenido, has logrado iniciar sesion de la biblioteca</h1>

        <a href="index.php?u=<?php echo $usuario ?>">Ir a la pagina web</a> 

        
        <a href="cerrar_login.php">Cerrar sesion</a>
   </div>
   
</body>
</html>